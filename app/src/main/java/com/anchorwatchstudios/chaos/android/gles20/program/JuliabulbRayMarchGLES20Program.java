package com.anchorwatchstudios.chaos.android.gles20.program;

import android.content.Context;
import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.android.gles20.shader.GLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.JuliabulbRayMarchGLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.VertexGLES20Shader;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Juliabulb;

import java.io.IOException;

/**
 * Renders a Julia set with OpenGL4.
 * 
 * @author Anthony Atella
 */
public class JuliabulbRayMarchGLES20Program extends RayMarchGLES20Program {

	private Context context;
	private Juliabulb juliabulb;
	private int iterations;

	/**
	 * Constructs a new <code>JuliaComplexPlotGLES20Program</code>.
	 */
	public JuliabulbRayMarchGLES20Program(Context context) {

		super();
		this.context = context;
		juliabulb = new Juliabulb();
		iterations = juliabulb.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Juliabulb) {
				
				juliabulb = (Juliabulb) fractal;
				
				if(juliabulb.getIterations() != iterations) {

					iterations = juliabulb.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected void passUniforms() {

		int c = GLES20.glGetUniformLocation(getId(), "c");
		GLES20.glUniform3f(c, (float) juliabulb.getC().getA(), (float) juliabulb.getC().getB(), (float) juliabulb.getC().getC());

		int n = GLES20.glGetUniformLocation(getId(), "n");
		GLES20.glUniform1f(n, (float) juliabulb.getN());

		int discrete = GLES20.glGetUniformLocation(getId(), "discrete");
		GLES20.glUniform1i(discrete, GLES20Program.boolToInt(juliabulb.getDiscrete()));

		int fractalThreshold = GLES20.glGetUniformLocation(getId(), "fractalThreshold");
		GLES20.glUniform1f(fractalThreshold, (float) juliabulb.getThreshold());
		
	}
	
	@Override
	protected GLES20Shader[] getShaders() throws IOException {

		return new GLES20Shader[] {
				new VertexGLES20Shader(context),
				new JuliabulbRayMarchGLES20Shader(context, iterations, getRayMarchIterations(), getShader().toString())
		};
		
	}
	
}
