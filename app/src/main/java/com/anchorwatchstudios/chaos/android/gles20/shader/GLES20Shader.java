package com.anchorwatchstudios.chaos.android.gles20.shader;

import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.gl.SimpleGLShader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

/**
 * An OpenGLES20 implementation of <code>SimpleShader</code>. Handles
 * the boilerplate involved in OpenGL4 shader programs.
 * 
 * @author Anthony Atella
 */
public class GLES20Shader extends SimpleGLShader {

	private static final int LOG_LENGTH = 2048;
	private int type;

	/**
	 * Constructs a new <code>GLES20Shader</code> with the given values.
	 * 
	 * @param program The program string array
	 * @param type GL shader type. Possible values are GL4.GL_FRAGMENT_SHADER,
	 * GL4.GL_VERTEX_SHADER ... etc
	 */
	public GLES20Shader(String[] program, int type) {

		super(program);
		this.type = type;

	}

	/**
	 * Constructs a new <code>GLES20Shader</code> with the given values.
	 * 
	 * @param is The input stream
	 * @param type GL shader type. Possible values are GL4.GL_FRAGMENT_SHADER,
	 * GL4.GL_VERTEX_SHADER ... etc
	 * @throws IOException If an IO error occurs
	 */
	public GLES20Shader(InputStream is, int type) throws IOException {

		super(is);
		this.type = type;

	}

	/**
	 * Constructs a new <code>GLES20Shader</code> with the given values.
	 * 
	 * @param is the input stream array
	 * @param type GL shader type. Possible values are GL4.GL_FRAGMENT_SHADER,
	 * GL4.GL_VERTEX_SHADER ... etc
	 * @throws IOException if an IO error occurs
	 */
	public GLES20Shader(InputStream[] is, int type) throws IOException {
		
		super(is);
		this.type = type;

	}

	/**
	 * Returns the GL4 shader type.
	 * 
	 * @return The GL4 shader type
	 */
	public int getType() {

		return type;

	}

	/**
	 * Sets the GL4 shader type.
	 * 
	 * @param type The new GL4 shader type
	 */
	public void setType(int type) {

		this.type = type;

	}

	@Override
	public boolean compile() {

		setId(GLES20.glCreateShader(type));
		GLES20.glShaderSource(getId(), getProgram()[0]);
		GLES20.glCompileShader(getId());
		int[] buffer = new int[1];
		GLES20.glGetShaderiv(getId(), GLES20.GL_COMPILE_STATUS, buffer, 0);
		return buffer[0] == GLES20.GL_TRUE;

	}

	@Override
	public String getLog() {

		ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 10);
		buffer.order(ByteOrder.nativeOrder());
		ByteBuffer tmp = ByteBuffer.allocateDirect(4);
		tmp.order(ByteOrder.nativeOrder());
		IntBuffer intBuffer = tmp.asIntBuffer();
		GLES20.glGetShaderInfoLog(getId());
		int numBytes = intBuffer.get(0);
		byte[] bytes = new byte[numBytes];
		buffer.get(bytes);
		return new String(bytes);

	}

}
