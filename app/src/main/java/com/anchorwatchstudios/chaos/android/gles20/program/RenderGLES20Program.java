package com.anchorwatchstudios.chaos.android.gles20.program;

import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.android.gles20.shader.GLES20Shader;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.KeyFrame;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Color;
import com.anchorwatchstudios.chaos.document.shader.DichromaticShader;
import com.anchorwatchstudios.chaos.document.shader.MinDistanceShader;
import com.anchorwatchstudios.chaos.document.shader.MonochromaticShader;
import com.anchorwatchstudios.chaos.document.shader.RandomShader;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.math.Vec2i;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * An abstract framework for rendering things in OpenGLES20. Shaders,
 * geometry, and render settings are provided with abstract methods.
 * 
 * @author Anthony Atella
 */
public abstract class RenderGLES20Program extends GLES20Program {

	private static final float[] VERTICES = {
			-1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0,0f
	};
	private static final int VERTEX_SIZE = 3;
	private int vbo;
	private boolean reload;
	private Shader shader;
	private String shaderClass;
	private RenderMethod renderMethod;
	private Vec2i resolution;

	/**
	 * Returns the current shader.
	 * 
	 * @return The current shader
	 */
	protected Shader getShader() {
		
		return shader;
		
	}

	/**
	 * Returns the current render method.
	 * 
	 * @return The current render method
	 */
	protected RenderMethod getRenderMethod() {
		
		return renderMethod;
		
	}

	/**
	 * Returns the current resolution.
	 * 
	 * @return The current resolution
	 */
	protected Vec2i getResolution() {
		
		return resolution;
		
	}

	/**
	 * Passes uniform values to the shader program.
	 */
	protected abstract void passUniforms();
	
	/**
	 * Passes uniform values to the shader program related
	 * to the render method.
	 * 
	 * @param renderMethod The render method
	 */
	protected abstract void passRenderUniforms(RenderMethod renderMethod);
	
	/**
	 * Passes uniforms to the shader related to color. Common for
	 * all render methods.
	 * 
	 * @param shader The shader being used
	 */
	protected void passShaderUniforms(Shader shader) {
		
		if(shader instanceof DichromaticShader) {
			
			DichromaticShader dichromatic = (DichromaticShader) shader;
			
			Color c1 = dichromatic.getColor1();
			Color c2 = dichromatic.getColor2();
			int color1 = GLES20.glGetUniformLocation(getId(), "color1");
			int color2 = GLES20.glGetUniformLocation(getId(), "color2");
			GLES20.glUniform4f(color1, (float) c1.getRed() / 255, (float) c1.getGreen() / 255, (float) c1.getBlue() / 255, (float) c1.getAlpha() / 255);
			GLES20.glUniform4f(color2, (float) c2.getRed() / 255, (float) c2.getGreen() / 255, (float) c2.getBlue() / 255, (float) c2.getAlpha() / 255);

			int inverted = GLES20.glGetUniformLocation(getId(), "inverted");
			GLES20.glUniform1i(inverted, boolToInt(dichromatic.isInverted()));
			
		}
		else if(shader instanceof MonochromaticShader) {
			
			MonochromaticShader mono = (MonochromaticShader) shader;
			
			Color col = mono.getColor1();
			int color1 = GLES20.glGetUniformLocation(getId(), "color1");
			GLES20.glUniform4f(color1, (float) col.getRed() / 255, (float) col.getGreen() / 255, (float) col.getBlue() / 255, (float) col.getAlpha() / 255);
			
			int inverted = GLES20.glGetUniformLocation(getId(), "inverted");
			GLES20.glUniform1i(inverted, boolToInt(mono.isInverted()));
			
		}
		else if(shader instanceof RandomShader) {
			
			RandomShader random = (RandomShader) shader;
			int inverted = GLES20.glGetUniformLocation(getId(), "inverted");
			GLES20.glUniform1i(inverted, boolToInt(random.isInverted()));
			
		}
		else if(shader instanceof MinDistanceShader) {
			
			MinDistanceShader artistic = (MinDistanceShader) shader;
			
			Color c1 = artistic.getColor1();
			Color c2 = artistic.getColor2();
			Color c3 = artistic.getColor3();
			int color1 = GLES20.glGetUniformLocation(getId(), "color1");
			int color2 = GLES20.glGetUniformLocation(getId(), "color2");
			int color3 = GLES20.glGetUniformLocation(getId(), "color3");
			GLES20.glUniform4f(color1, (float) c1.getRed() / 255, (float) c1.getGreen() / 255, (float) c1.getBlue() / 255, (float) c1.getAlpha() / 255);
			GLES20.glUniform4f(color2, (float) c2.getRed() / 255, (float) c2.getGreen() / 255, (float) c2.getBlue() / 255, (float) c2.getAlpha() / 255);
			GLES20.glUniform4f(color3, (float) c3.getRed() / 255, (float) c3.getGreen() / 255, (float) c3.getBlue() / 255, (float) c3.getAlpha() / 255);

			int inverted = GLES20.glGetUniformLocation(getId(), "inverted");
			GLES20.glUniform1i(inverted, boolToInt(artistic.isInverted()));
			
		}
		
	}
	
	/**
	 * Returns the shaders that this program will use.
	 * 
	 * @return The shaders this program will use
	 * @throws IOException 
	 */
	protected abstract GLES20Shader[] getShaders() throws IOException;
	
	/**
	 * Loads the necessary shaders from files.
	 * 
	 * @throws IOException If the files cannot be found
	 */
	private void loadShaders() throws IOException {

		GLES20Shader[] shaders = getShaders();

		for(int i = 0;i < shaders.length;i++) {
			
			shaders[i].compile();
			shaders[i].getType();
			attachShader(shaders[i]);
			
		}

		linkAndValidate();

	}
	
	@Override
	public void init() {

		super.init();

		try {

			loadShaders();
			
		}
		catch (IOException e) {

			e.printStackTrace();
		
		}

		// Init the VBO for vertices
		int[] b = new int[1];
		IntBuffer buffer = ByteBuffer.allocateDirect(Integer.BYTES).asIntBuffer();
		GLES20.glGenBuffers(1, buffer);
		vbo = buffer.get(0);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo);
		FloatBuffer fb = FloatBuffer.allocate(VERTICES.length);
		fb.put(VERTICES);
		fb.rewind();
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, VERTICES.length * Float.BYTES, fb, GLES20.GL_STATIC_DRAW);

	}

	@Override
	public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {

		init();
		GLES20.glClearColor(0.0f, 0.0f, 0.5f, 1.0f);

	}

	@Override
	public void onSurfaceChanged(GL10 gl10, int i, int i1) {

		//GLES20.glViewport(0, 0, width, height);

	}

	@Override
	public void onDrawFrame(GL10 gl10) {

		if(reload) {

			delete();
			super.init();

			try {

				loadShaders();

			}
			catch (IOException e) {

				e.printStackTrace();

			}

			reload = false;

		}

		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		GLES20.glUseProgram(getId());
		int p = GLES20.glGetAttribLocation(getId(), "position");
		//System.out.println(getId());
		GLES20.glEnableVertexAttribArray(p);
		passUniforms();
		passShaderUniforms(shader);
		passRenderUniforms(renderMethod);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo);
		GLES20.glVertexAttribPointer(p, VERTEX_SIZE, GLES20.GL_FLOAT, false, 0, 0);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, VERTICES.length / VERTEX_SIZE);

	}

	@Override
	public void reload() {

		reload = true;
		
	}
	
	@Override
	public void notify(DocumentManager documentManager) {

		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			KeyFrame keyframe = document.getCurrentKeyFrame();
			shader = keyframe.getShader();
			renderMethod = keyframe.getRenderMethod();
			resolution = document.getSettings().getResolution();
			String m = keyframe.getShader().toString();
			
			if(shaderClass != m) {
				
				shaderClass = m;
				reload();
				
			}	
			
		}
		
	}

}
