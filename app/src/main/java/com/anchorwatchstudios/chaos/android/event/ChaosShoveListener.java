package com.anchorwatchstudios.chaos.android.event;

import com.almeros.android.multitouch.ShoveGestureDetector;
import com.almeros.android.multitouch.ShoveGestureDetector.SimpleOnShoveGestureListener;
import com.anchorwatchstudios.chaos.android.Chaos;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Julia;
import com.anchorwatchstudios.chaos.document.fractal.Juliabulb;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec2i;

/**
 * Created by P40 on 3/22/2018.
 */

public class ChaosShoveListener extends SimpleOnShoveGestureListener {

    private Chaos context;

    public ChaosShoveListener(Chaos context) {

        this.context = context;

    }

    @Override
    public boolean onShove(ShoveGestureDetector s) {

        FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

        if(document != null) {

            Fractal f = document.getCurrentKeyFrame().getFractal();
            Vec2i resolution = document.getSettings().getResolution();
            double percent = s.getShovePixelsDelta() / resolution.getY();

            if (f instanceof Julia) {

                Julia j = (Julia) f;
                j.setC(new Complex(j.getC().getA() + percent, j.getC().getB()));

            }
            else if(f instanceof Juliabulb) {

                Juliabulb ju = (Juliabulb) f;
                double n = ju.getN();
                ju.setN(n - percent);

            }

            document.setEdited(true);
            context.getDocumentManager().notifyObservers();
            return true;

        }

        return false;

    }

}
