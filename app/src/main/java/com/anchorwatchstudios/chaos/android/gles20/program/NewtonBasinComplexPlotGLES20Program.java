package com.anchorwatchstudios.chaos.android.gles20.program;

import android.content.Context;
import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.android.gles20.shader.GLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.NewtonBasinComplexPlotGLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.VertexGLES20Shader;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.NewtonBasin;

import java.io.IOException;

/**
 * Renders a Julia set with OpenGL4.
 * 
 * @author Anthony Atella
 */
public class NewtonBasinComplexPlotGLES20Program extends ComplexPlotGLES20Program {

	private Context context;
	private NewtonBasin newtonBasin;
	private int iterations;

	/**
	 * Constructs a new <code>JuliaComplexPlotGLES20Program</code>.
	 */
	public NewtonBasinComplexPlotGLES20Program(Context context) {

		super();
		this.context = context;
		newtonBasin = new NewtonBasin();
		iterations = newtonBasin.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof NewtonBasin) {
				
				newtonBasin = (NewtonBasin) fractal;
				
				if(newtonBasin.getIterations() != iterations) {

					iterations = newtonBasin.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected void passUniforms() {

		int alpha = GLES20.glGetUniformLocation(getId(), "alpha");
		GLES20.glUniform2f(alpha, (float) newtonBasin.getAlpha().getA(), (float) newtonBasin.getAlpha().getB());

		int n = GLES20.glGetUniformLocation(getId(), "n");
		GLES20.glUniform2f(n, (float) newtonBasin.getN().getA(), (float) newtonBasin.getN().getB());

		int fractalThreshold = GLES20.glGetUniformLocation(getId(), "fractalThreshold");
		GLES20.glUniform1f(fractalThreshold, (float) newtonBasin.getThreshold());
		
	}
	
	@Override
	protected GLES20Shader[] getShaders() throws IOException {

		return new GLES20Shader[] {
				new VertexGLES20Shader(context),
				new NewtonBasinComplexPlotGLES20Shader(context, iterations, getShader().toString())
		};
		
	}
	
}
