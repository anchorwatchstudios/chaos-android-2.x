package com.anchorwatchstudios.chaos.android.gles20.program;

import android.content.Context;
import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.android.gles20.shader.GLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.MandelbulbRayMarchGLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.VertexGLES20Shader;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbulb;

import java.io.IOException;

/**
 * Renders a Julia set with OpenGL4.
 * 
 * @author Anthony Atella
 */
public class MandelbulbRayMarchGLES20Program extends RayMarchGLES20Program {

	private Context context;
	private Mandelbulb mandelbulb;
	private int iterations;

	/**
	 * Constructs a new <code>JuliaComplexPlotGLES20Program</code>.
	 */
	public MandelbulbRayMarchGLES20Program(Context context) {

		super();
		this.context = context;
		mandelbulb = new Mandelbulb();
		iterations = mandelbulb.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Mandelbulb) {
				
				mandelbulb = (Mandelbulb) fractal;
				
				if(mandelbulb.getIterations() != iterations) {

					iterations = mandelbulb.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected void passUniforms() {

		int z = GLES20.glGetUniformLocation(getId(), "z");
		GLES20.glUniform3f(z, (float) mandelbulb.getZ().getA(), (float) mandelbulb.getZ().getB(), (float) mandelbulb.getZ().getC());

		int n = GLES20.glGetUniformLocation(getId(), "n");
		GLES20.glUniform1f(n, (float) mandelbulb.getN());

		int discrete = GLES20.glGetUniformLocation(getId(), "discrete");
		GLES20.glUniform1i(discrete, GLES20Program.boolToInt(mandelbulb.getDiscrete()));

		int fractalThreshold = GLES20.glGetUniformLocation(getId(), "fractalThreshold");
		GLES20.glUniform1f(fractalThreshold, (float) mandelbulb.getThreshold());
		
	}
	
	@Override
	protected GLES20Shader[] getShaders() throws IOException {

		return new GLES20Shader[] {
				new VertexGLES20Shader(context),
				new MandelbulbRayMarchGLES20Shader(context, iterations, getRayMarchIterations(), getShader().toString())
		};
		
	}
	
}
