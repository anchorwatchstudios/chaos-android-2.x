package com.anchorwatchstudios.chaos.android.canvas;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import com.anchorwatchstudios.chaos.android.gles20.program.GLES20Program;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.document.FRC2Document;

public class GLES20Canvas extends GLSurfaceView implements DocumentManagerObserver {

    private GLES20Program program;

    public GLES20Canvas(Context context) {

        super(context);
        setEGLContextClientVersion(2);

    }

    public GLES20Canvas(Context context, AttributeSet attrs) {

        super(context, attrs);
        setEGLContextClientVersion(2);

    }

    public GLES20Canvas(Context context, GLES20Program program) {

        super(context);
        this.program = program;

    }

    @Override
    public void notify(DocumentManager documentManager) {

        FRC2Document document = (FRC2Document) documentManager.getCurrent();

        if(document != null && program != null) {

            program.onDrawFrame(null);

        }

    }

    public GLES20Program getProgram() {

        return program;

    }

    public void setProgram(GLES20Program program) {

        this.program = program;
        setRenderer(program);

    }

}