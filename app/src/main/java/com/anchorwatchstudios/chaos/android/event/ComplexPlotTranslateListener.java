package com.anchorwatchstudios.chaos.android.event;

import android.content.Context;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

import com.anchorwatchstudios.chaos.android.Chaos;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec2i;

public class ComplexPlotTranslateListener extends SimpleOnGestureListener {

    private Chaos context;

    public ComplexPlotTranslateListener(Context context) {

        this.context = (Chaos) context;

    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

        FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

        if(document != null) {

            RenderMethod rm = document.getCurrentKeyFrame().getRenderMethod();

            if (rm instanceof ComplexPlot && e1.getPointerCount() == 1 && e2.getPointerCount() == 1) {

                // TODO: Rotation isn't perfect...    8..(
                ComplexPlot cp = (ComplexPlot) rm;
                Vec2i resolution = document.getSettings().getResolution();
                double xPercent = distanceX / resolution.getX();
                double yPercent = distanceY / resolution.getY();
                Complex r = new Complex(xPercent, yPercent).rotate(cp.getRotation());
                cp.pan(r.getA(), r.getB());
                document.setEdited(true);
                document.notifyObservers();
                return true;

            }

        }

        return false;

    }

}