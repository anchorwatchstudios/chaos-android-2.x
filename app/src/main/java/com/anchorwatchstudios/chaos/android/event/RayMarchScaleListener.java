package com.anchorwatchstudios.chaos.android.event;

import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;

import com.anchorwatchstudios.chaos.android.Chaos;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Vec3d;

/**
 * Created by P40 on 3/22/2018.
 */

public class RayMarchScaleListener extends SimpleOnScaleGestureListener {

    private Chaos context;

    public RayMarchScaleListener(Chaos context) {

        this.context = context;

    }

    @Override
    public boolean onScale(ScaleGestureDetector s) {

        super.onScale(s);
        FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

        if(document != null) {

            RenderMethod rm = document.getCurrentKeyFrame().getRenderMethod();

            if(rm instanceof RayMarch) {

                RayMarch rayMarch = (RayMarch) rm;
                Vec3d lookAt = rayMarch.getCameraLookAt();
                Vec3d origin = rayMarch.getCameraOrigin();
                rayMarch.setCameraOrigin(origin.add(lookAt.scalar(s.getScaleFactor() - 1.0)));
                return true;

            }

        }

        return false;

    }

}
