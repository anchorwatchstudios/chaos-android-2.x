package com.anchorwatchstudios.chaos.android.event;

import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;

import com.anchorwatchstudios.chaos.android.Chaos;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;

/**
 * Created by P40 on 3/22/2018.
 */

public class ComplexPlotScaleListener extends SimpleOnScaleGestureListener {

    private static final double SENSITIVITY = 0.1;
    private Chaos context;

    public ComplexPlotScaleListener(Chaos context) {

        this.context = context;

    }

    @Override
    public boolean onScale(ScaleGestureDetector s) {

        super.onScale(s);
        FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

        if(document != null) {

            RenderMethod rm = document.getCurrentKeyFrame().getRenderMethod();

            if(rm instanceof ComplexPlot) {

                ComplexPlot cp = (ComplexPlot) rm;
                cp.zoom(1.0f - s.getScaleFactor());
                return true;

            }

        }

        return false;

    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector s) {

        return true;

    }

    @Override
    public void onScaleEnd(ScaleGestureDetector s) {

    }

}
