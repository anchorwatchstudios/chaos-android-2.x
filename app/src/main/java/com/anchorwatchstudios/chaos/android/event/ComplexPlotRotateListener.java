package com.anchorwatchstudios.chaos.android.event;

import com.almeros.android.multitouch.RotateGestureDetector;
import com.almeros.android.multitouch.RotateGestureDetector.SimpleOnRotateGestureListener;
import com.anchorwatchstudios.chaos.android.Chaos;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;

/**
 * Created by P40 on 3/23/2018.
 */

public class ComplexPlotRotateListener extends SimpleOnRotateGestureListener {

    private Chaos context;

    public ComplexPlotRotateListener(Chaos context) {

        this.context = context;

    }

    @Override
    public boolean onRotate(RotateGestureDetector r) {

        FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

        if(document != null) {

            RenderMethod rm = document.getCurrentKeyFrame().getRenderMethod();

            if(rm instanceof ComplexPlot) {

                ComplexPlot cp = (ComplexPlot) rm;
                double delta = r.getRotationDegreesDelta() * Math.PI / 180;
                cp.setRotation(cp.getRotation() + delta);
                document.setEdited(true);
                context.getDocumentManager().notifyObservers();
                return true;

            }

        }

        return false;

    }

}
