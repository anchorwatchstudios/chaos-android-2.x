package com.anchorwatchstudios.chaos.android.gles20.program;

import android.content.Context;
import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.android.gles20.shader.GLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.JuliaComplexPlotGLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.VertexGLES20Shader;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Julia;

import java.io.IOException;

/**
 * Renders a Julia set with OpenGL4.
 * 
 * @author Anthony Atella
 */
public class JuliaComplexPlotGLES20Program extends ComplexPlotGLES20Program {

	private Context context;
	private Julia julia;
	private int iterations;
	
	/**
	 * Constructs a new <code>JuliaComplexPlotGLES20Program</code>.
	 */
	public JuliaComplexPlotGLES20Program(Context context) {

		super();
		this.context = context;
		julia = new Julia();
		iterations = julia.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Julia) {
				
				julia = (Julia) fractal;
				
				if(julia.getIterations() != iterations) {

					iterations = julia.getIterations();
					reload();
					
				}
				
			}
			
		}

	}

	@Override
	protected void passUniforms() {

		int c = GLES20.glGetUniformLocation(getId(), "c");
		GLES20.glUniform2f(c, (float) julia.getC().getA(), (float) julia.getC().getB());
		
		int discrete = GLES20.glGetUniformLocation(getId(), "discrete");
		GLES20.glUniform1i(discrete, GLES20Program.boolToInt(julia.getDiscrete()));
		
		int fractalThreshold = GLES20.glGetUniformLocation(getId(), "fractalThreshold");
		GLES20.glUniform1f(fractalThreshold, (float) julia.getThreshold());
		
	}
	
	@Override
	protected GLES20Shader[] getShaders() throws IOException {

		return new GLES20Shader[] {
				new VertexGLES20Shader(context),
				new JuliaComplexPlotGLES20Shader(context, iterations, getShader().toString())
		};
		
	}
	
}
