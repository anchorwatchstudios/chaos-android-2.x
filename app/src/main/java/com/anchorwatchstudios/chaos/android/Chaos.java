package com.anchorwatchstudios.chaos.android;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.Window;
import android.view.WindowManager;

import com.almeros.android.multitouch.RotateGestureDetector;
import com.almeros.android.multitouch.ShoveGestureDetector;
import com.anchorwatchstudios.chaos.android.canvas.GLES20Canvas;
import com.anchorwatchstudios.chaos.android.event.ChaosShoveListener;
import com.anchorwatchstudios.chaos.android.event.ComplexPlotRotateListener;
import com.anchorwatchstudios.chaos.android.event.ComplexPlotScaleListener;
import com.anchorwatchstudios.chaos.android.event.ComplexPlotTranslateListener;
import com.anchorwatchstudios.chaos.android.event.RayMarchScaleListener;
import com.anchorwatchstudios.chaos.android.event.RayMarchTranslateListener;
import com.anchorwatchstudios.chaos.android.gles20.program.JuliaComplexPlotGLES20Program;
import com.anchorwatchstudios.chaos.document.ArrayListDocumentManager;
import com.anchorwatchstudios.chaos.document.ArrayListFRC2Document;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.Settings;
import com.anchorwatchstudios.chaos.document.fractal.Julia;
import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.shader.Color;
import com.anchorwatchstudios.chaos.document.shader.MonochromaticShader;
import com.anchorwatchstudios.chaos.math.Vec2i;

public class Chaos extends AppCompatActivity {

    public static final int VERSION_MAJOR = 1;
    public static final int VERSION_MINOR = 0;
    public static final int VERSION_PATCH = 0;
    private DocumentManager documentManager;
    private ScaleGestureDetector cpScaleDetector;
    private ScaleGestureDetector rmScaleDetector;
    private RotateGestureDetector cpRotateDetector;
    private GestureDetector cpMoveDetector;
    private GestureDetector rmMoveDetector;
    private ShoveGestureDetector shoveDetector;

    public DocumentManager getDocumentManager() {

        return documentManager;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_chaos);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        cpMoveDetector = new GestureDetector(this, new ComplexPlotTranslateListener(this));
        rmMoveDetector = new GestureDetector(this, new RayMarchTranslateListener(this));
        cpScaleDetector = new ScaleGestureDetector(this, new ComplexPlotScaleListener(this));
        rmScaleDetector = new ScaleGestureDetector(this, new RayMarchScaleListener(this));
        shoveDetector = new ShoveGestureDetector(this, new ChaosShoveListener(this));
        cpRotateDetector = new RotateGestureDetector(this, new ComplexPlotRotateListener(this));
        GLES20Canvas glView = findViewById(R.id.glCanvas);
        JuliaComplexPlotGLES20Program renderer = new JuliaComplexPlotGLES20Program(this);
        //JuliabulbRayMarchGLES20Program renderer = new JuliabulbRayMarchGLES20Program(this);
        glView.setProgram(renderer);
        documentManager = new ArrayListDocumentManager();
        documentManager.registerObserver(renderer);
        documentManager.registerObserver(glView);
        Settings settings = new Settings();
        Point p = new Point();
        getWindowManager().getDefaultDisplay().getSize(p);
        settings.setResolution(new Vec2i(p.x, p.y));
        MonochromaticShader shader = new MonochromaticShader();
        shader.setColor1(new Color(0, 255, 0, 255));
        documentManager.add(new ArrayListFRC2Document(new Julia(), new ComplexPlot(), settings, shader));
        //documentManager.add(new ArrayListFRC2Document(new Juliabulb(), new RayMarch(), settings, new MonochromaticShader()));

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        super.onTouchEvent(event);
        shoveDetector.onTouchEvent(event);

        if(!shoveDetector.isInProgress()) {

            cpScaleDetector.onTouchEvent(event);
            cpRotateDetector.onTouchEvent(event);
            rmScaleDetector.onTouchEvent(event);

        }

        if(!cpScaleDetector.isInProgress() && !cpRotateDetector.isInProgress() && !rmScaleDetector.isInProgress()) {

            cpMoveDetector.onTouchEvent(event);
            rmMoveDetector.onTouchEvent(event);

        }

        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        //MenuItem shareItem = menu.findItem(R.id.share_button);
        //ShareActionProvider shareActionProvider =
        //        (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        //Intent intent = new Intent(Intent.ACTION_SEND);
        //intent.setType("image/*");
        // TODO: DATA SHARE
        //intent.putExtra(Intent.EXTRA_STREAM, "");
        //shareActionProvider.setShareIntent(intent);
        return true;

    }

}
