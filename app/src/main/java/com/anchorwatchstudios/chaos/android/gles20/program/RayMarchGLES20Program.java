package com.anchorwatchstudios.chaos.android.gles20.program;

import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.anchorwatchstudios.chaos.math.Vec3d;

/**
 * Renders geometry on the complex plane.
 * 
 * @author Anthony Atella
 */
public abstract class RayMarchGLES20Program extends RenderGLES20Program {

	private int rayMarchIterations;

	/**
	 * Returns the current number of raymarch iterations.
	 *
	 * @return The current number of raymarch iterations
	 */
	public int getRayMarchIterations() {

		return rayMarchIterations;

	}

	@Override
	public void passRenderUniforms(RenderMethod rm) {

		if(rm instanceof RayMarch) {

			Vec2i resolution = getResolution();
			RayMarch renderMethod = (RayMarch) rm;
			int res = GLES20.glGetUniformLocation(getId(), "resolution");
			GLES20.glUniform2f(res, (float) resolution.getX(), (float) resolution.getY());

			double r = renderMethod.getRayThreshold();
			int rayThreshold = GLES20.glGetUniformLocation(getId(), "rayThreshold");
			GLES20.glUniform1f(rayThreshold, (float) r);

			Vec3d cOrigin = renderMethod.getCameraOrigin();
			int cameraOrigin = GLES20.glGetUniformLocation(getId(), "cameraOrigin");
			GLES20.glUniform3f(cameraOrigin, (float) cOrigin.getX(), (float) cOrigin.getY(), (float) cOrigin.getZ());

			Vec3d cUp = renderMethod.getCameraUp();
			int cameraUp = GLES20.glGetUniformLocation(getId(), "cameraUp");
			GLES20.glUniform3f(cameraUp, (float) cUp.getX(), (float) cUp.getY(), (float) cUp.getZ());

			Vec3d cLookAt = renderMethod.getCameraLookAt();
			int cameraLookAt = GLES20.glGetUniformLocation(getId(), "cameraLookAt");
			GLES20.glUniform3f(cameraLookAt, (float) cLookAt.getX(), (float) cLookAt.getY(), (float) cLookAt.getZ());

		}
		
	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();

		if(document != null) {

			RayMarch rm = (RayMarch) getRenderMethod();

			if(rm.getIterations() != rayMarchIterations) {

				rayMarchIterations = rm.getIterations();
				reload();

			}

		}

	}
	
}
