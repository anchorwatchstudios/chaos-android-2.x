package com.anchorwatchstudios.chaos.android.gles20.program;

import android.content.Context;
import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.android.gles20.shader.GLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.MandelboxRayMarchGLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.VertexGLES20Shader;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbox;

import java.io.IOException;

/**
 * Renders a Julia set with OpenGL4.
 * 
 * @author Anthony Atella
 */
public class MandelboxRayMarchGLES20Program extends RayMarchGLES20Program {

	private Context context;
	private Mandelbox mandelBox;
	private int iterations;

	/**
	 * Constructs a new <code>JuliaComplexPlotGLES20Program</code>.
	 */
	public MandelboxRayMarchGLES20Program(Context context) {

		super();
		this.context = context;
		mandelBox = new Mandelbox();
		iterations = mandelBox.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Mandelbox) {
				
				mandelBox = (Mandelbox) fractal;
				
				if(mandelBox.getIterations() != iterations) {

					iterations = mandelBox.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected void passUniforms() {

		int fractalThreshold = GLES20.glGetUniformLocation(getId(), "fractalThreshold");
		GLES20.glUniform1f(fractalThreshold, (float) mandelBox.getThreshold());

		int foldLimit = GLES20.glGetUniformLocation(getId(), "foldLimit");
		GLES20.glUniform1f(foldLimit, (float) mandelBox.getFoldLimit());

		int minRadius = GLES20.glGetUniformLocation(getId(), "minRadius");
		GLES20.glUniform1f(minRadius, (float) mandelBox.getMinRadius());

		int fixedRadius = GLES20.glGetUniformLocation(getId(), "fixedRadius");
		GLES20.glUniform1f(fixedRadius, (float) mandelBox.getFixedRadius());

		int scale = GLES20.glGetUniformLocation(getId(), "scale");
		GLES20.glUniform1f(scale, (float) mandelBox.getScale());
		
	}
	
	@Override
	protected GLES20Shader[] getShaders() throws IOException {

		return new GLES20Shader[] {
				new VertexGLES20Shader(context),
				new MandelboxRayMarchGLES20Shader(context, iterations, getRayMarchIterations(), getShader().toString())
		};
		
	}
	
}
