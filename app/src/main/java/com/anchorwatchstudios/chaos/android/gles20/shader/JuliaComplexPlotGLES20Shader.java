package com.anchorwatchstudios.chaos.android.gles20.shader;

import android.content.Context;
import android.opengl.GLES20;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Loads the shaders necessary to run the julia program.
 * 
 * @author Anthony Atella
 */
public class JuliaComplexPlotGLES20Shader extends GLES20Shader {

	/**
	 * Constructs a new <code>JuliaComplexPlotGL4Shader</code> with the given values.
	 *
	 * @param context The android context
	 * @param iterations The number of fractal iterations
	 * @param fragShaderName The unqualified name of the fragment shader
	 * @throws IOException If the shader files cannot be found
	 */
	public JuliaComplexPlotGLES20Shader(Context context, int iterations, String fragShaderName) throws IOException {

		super(new InputStream[] {
				context.getAssets().open("shader/render/complex-plot.glsl"),
				context.getAssets().open("shader/math/complex.glsl"),
				context.getAssets().open("shader/color/" + fragShaderName +".glsl")
		}, GLES20.GL_FRAGMENT_SHADER);
		InputStreamReader isr = new InputStreamReader(context.getAssets().open("shader/geom/julia.glsl"));
		BufferedReader br = new BufferedReader(isr);
		StringBuilder sb = new StringBuilder();
		String line;

		while((line = br.readLine()) != null) {

			if(line.contains("#define FRACTAL_ITERATIONS")) {
			
				sb.append("#define FRACTAL_ITERATIONS " + iterations + "\n");
				
			}
			else {
				
				sb.append(line + "\n");

			}			

		}
		
		isr.close();
		br.close();
		
		setProgram(new String[] { getProgram()[0] + sb.toString() });

	}
	
}
