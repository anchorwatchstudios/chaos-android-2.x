package com.anchorwatchstudios.chaos.android.gles20.program;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.anchorwatchstudios.chaos.android.AndroidProgram;
import com.anchorwatchstudios.chaos.gl.GLShader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

/**
 * An abstraction of an OpenGL4 program. Contains common
 * boilerplate implementation of OpenGL4 shader program calls.
 * 
 * @author Anthony Atella
 */
public abstract class GLES20Program implements AndroidProgram, GLSurfaceView.Renderer {

	private static final int LOG_LENGTH = 2048;
	private int id;
	
	/**
	 * Constructs a new <code>GL4Program</code> with an id of -1.
	 */
	public GLES20Program() {

		id = -1;
		
	}
	
	/**
	 * Constructs a new <code>GL4Program</code> and attaches the given
	 * shaders.
	 * 
	 * @param shaders The shader array
	 */
	public GLES20Program(GLShader[] shaders) {
		
		this();
		
		for(int i = 0;i < shaders.length;i++) {
			
			attachShader(shaders[i]);
			
		}

		init();

	}
	
	/**
	 * Converts a boolean to a 0 or a 1.
	 * 
	 * @param bool The boolean
	 * @return 0 if false, 1 if true
	 */
	public static int boolToInt(boolean bool) {
		
		if(bool) {
			
			return 1;
			
		}
		
		return 0;
		
	}

	/**
	 * Initializes the program and requests
	 * an ID from OpenGL.
	 */
	public void init() {

		id = GLES20.glCreateProgram();

	}

	@Override
	public int getId() {
		
		return id;
		
	}

	@Override
	public void attachShader(GLShader glShader) {

		GLES20.glAttachShader(id, glShader.getId());
		
	}
	
	@Override
	public boolean linkAndValidate() {

		GLES20.glLinkProgram(id);
		GLES20.glValidateProgram(id);
	    int[] buffer = new int[1];
		GLES20.glGetProgramiv(getId(), GLES20.GL_LINK_STATUS, buffer, 0);
	    return buffer[0] == GLES20.GL_TRUE;
		
	}
	
	@Override
	public String getLog() {

		ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 10);
		buffer.order(ByteOrder.nativeOrder());
		ByteBuffer tmp = ByteBuffer.allocateDirect(4);
		tmp.order(ByteOrder.nativeOrder());
		IntBuffer intBuffer = tmp.asIntBuffer();
		GLES20.glGetProgramInfoLog(getId());
		int numBytes = intBuffer.get(0);
		byte[] bytes = new byte[numBytes];
		buffer.get(bytes);
		return new String(bytes);

	}

	@Override
	public void delete() {

		GLES20.glDeleteProgram(getId());
		id = -1;
		
	}
		
}
