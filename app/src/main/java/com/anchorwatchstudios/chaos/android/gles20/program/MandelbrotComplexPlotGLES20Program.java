package com.anchorwatchstudios.chaos.android.gles20.program;

import android.content.Context;
import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.android.gles20.shader.GLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.MandelbrotComplexPlotGLES20Shader;
import com.anchorwatchstudios.chaos.android.gles20.shader.VertexGLES20Shader;
import com.anchorwatchstudios.chaos.document.DocumentManager;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.fractal.Mandelbrot;

import java.io.IOException;

/**
 * Renders a Julia set with OpenGL4.
 * 
 * @author Anthony Atella
 */
public class MandelbrotComplexPlotGLES20Program extends ComplexPlotGLES20Program {

	private Context context;
	private Mandelbrot mandelbrot;
	private int iterations;

	/**
	 * Constructs a new <code>JuliaComplexPlotGLES20Program</code>.
	 */
	public MandelbrotComplexPlotGLES20Program(Context context) {

		super();
		this.context = context;
		mandelbrot = new Mandelbrot();
		iterations = mandelbrot.getIterations();

	}

	@Override
	public void notify(DocumentManager documentManager) {

		super.notify(documentManager);
		FRC2Document document = (FRC2Document) documentManager.getCurrent();
		
		if(document != null) {
			
			Fractal fractal = document.getCurrentKeyFrame().getFractal();
			
			if(fractal instanceof Mandelbrot) {
				
				mandelbrot = (Mandelbrot) fractal;
				
				if(mandelbrot.getIterations() != iterations) {

					iterations = mandelbrot.getIterations();
					reload();
					
				}
				
			}
			
		}
		
	}

	@Override
	protected void passUniforms() {

		int z = GLES20.glGetUniformLocation(getId(), "z");
		GLES20.glUniform2f(z, (float) mandelbrot.getZ().getA(), (float) mandelbrot.getZ().getB());
		
		int discrete = GLES20.glGetUniformLocation(getId(), "discrete");
		GLES20.glUniform1i(discrete, GLES20Program.boolToInt(mandelbrot.getDiscrete()));
		
		int fractalThreshold = GLES20.glGetUniformLocation(getId(), "fractalThreshold");
		GLES20.glUniform1f(fractalThreshold, (float) mandelbrot.getThreshold());
		
	}
	
	@Override
	protected GLES20Shader[] getShaders() throws IOException {

		return new GLES20Shader[] {
				new VertexGLES20Shader(context),
				new MandelbrotComplexPlotGLES20Shader(context, iterations, getShader().toString())
		};
		
	}
	
}
