package com.anchorwatchstudios.chaos.android.event;

import android.content.Context;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

import com.anchorwatchstudios.chaos.android.Chaos;
import com.anchorwatchstudios.chaos.document.FRC2Document;
import com.anchorwatchstudios.chaos.document.render.RayMarch;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Vec2i;
import com.anchorwatchstudios.chaos.math.Vec3d;

public class RayMarchTranslateListener extends SimpleOnGestureListener {

    private static final Vec3d WORLD_UP = new Vec3d(0, 1, 0);
    private Chaos context;
    private double pitch;
    private double rotation;

    public RayMarchTranslateListener(Context context) {

        this.context = (Chaos) context;
        this.rotation = Math.PI * 1.5;
        this.pitch = Math.PI / 2;

    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

        FRC2Document document = (FRC2Document) context.getDocumentManager().getCurrent();

        if(document != null) {

            RenderMethod rm = document.getCurrentKeyFrame().getRenderMethod();

            if (rm instanceof RayMarch && e1.getPointerCount() == 1 && e2.getPointerCount() == 1) {

                RayMarch rayMarch = (RayMarch) rm;
                Vec2i resolution = document.getSettings().getResolution();
                double xDiff = distanceX / resolution.getX();
                double yDiff = distanceY / resolution.getY();
                rotation += xDiff * Math.PI / 4;

                if((pitch < Math.PI && yDiff > 0) || (pitch > 0 && yDiff < 0)) {

                    pitch += yDiff * Math.PI / 2;

                }

                Vec3d lookAt = new Vec3d(Math.cos(rotation), Math.cos(pitch), Math.sin(rotation)).normalize();
                rayMarch.setCameraLookAt(lookAt);
                Vec3d right = lookAt.cross(WORLD_UP);
                Vec3d up = right.cross(lookAt);
                rayMarch.setCameraUp(up);
                document.setEdited(true);
                document.notifyObservers();
                return true;

            }

        }

        return false;

    }

}