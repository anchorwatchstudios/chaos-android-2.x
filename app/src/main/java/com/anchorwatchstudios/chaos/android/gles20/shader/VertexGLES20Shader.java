package com.anchorwatchstudios.chaos.android.gles20.shader;

import android.content.Context;
import android.opengl.GLES20;

import java.io.IOException;

/**
 * Loads the shaders necessary to run the default vertex program.
 * 
 * @author Anthony Atella
 */
public class VertexGLES20Shader extends GLES20Shader {

	/**
	 * Constructs a new <code>VertexGLES20Shader</code> with the given values.
	 *
	 * @param context The Android context
	 * @throws IOException If the shader files cannot be found
	 */
	public VertexGLES20Shader(Context context) throws IOException {
	
		super(context.getAssets().open("shader/vertex/vertex.glsl"), GLES20.GL_VERTEX_SHADER);
		
	}
	
}
