package com.anchorwatchstudios.chaos.android.gles20.program;

import android.opengl.GLES20;

import com.anchorwatchstudios.chaos.document.render.ComplexPlot;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec2i;

/**
 * Renders geometry on the complex plane.
 * 
 * @author Anthony Atella
 */
public abstract class ComplexPlotGLES20Program extends RenderGLES20Program {
	
	@Override
	public void passRenderUniforms(RenderMethod renderMethod) {
		
		if(renderMethod instanceof ComplexPlot) {
		
			int res = GLES20.glGetUniformLocation(getId(), "resolution");
			Vec2i resolution = getResolution();
			GLES20.glUniform2f(res, (float) resolution.getX(), (float) resolution.getY());
			
			Complex c0 = ((ComplexPlot) renderMethod).getMin();
			Complex c1 = ((ComplexPlot) renderMethod).getMax();
			int cameraPos = GLES20.glGetUniformLocation(getId(), "cameraPos");
			GLES20.glUniformMatrix2fv(cameraPos, 1, false, new float[] { (float) c0.getA(), (float) c0.getB(), (float) c1.getA(), (float) c1.getB() }, 0);
			
			ComplexPlot complexPlot = (ComplexPlot) renderMethod;
			int rotation = GLES20.glGetUniformLocation(getId(), "rotation");
			GLES20.glUniform1f(rotation, (float) complexPlot.getRotation());
			
		}
		
	}
	
}
