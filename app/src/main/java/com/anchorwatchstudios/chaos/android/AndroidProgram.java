package com.anchorwatchstudios.chaos.android;

import com.anchorwatchstudios.chaos.document.DocumentManagerObserver;
import com.anchorwatchstudios.chaos.gl.GLShader;

/**
 * An OpenGL shader program in Android.
 * 
 * @author Anthony Atella
 */
public interface AndroidProgram extends DocumentManagerObserver {

	/**
	 * Returns the program id.
	 * 
	 * @return The program id
	 */
	int getId();
	
	/**
	 * Attaches the given shader to this program.
	 * 
	 * @param glShader The shader to attach
	 */
	void attachShader(GLShader glShader);
	
	/**
	 * Links and validates this program. Returns true
	 * if successful, false otherwise.
	 * 
	 * @return True if successful, false otherwise
	 */
	boolean linkAndValidate();
	
	/**
	 * Returns this programs log from the given GL context.
	 * 
	 * @return This programs log
	 */
	String getLog();
	
	/**
	 * Reloads this program.
	 */
	void reload();
	
	/**
	 * Deletes this program from the given context.
	 */
	void delete();
	
}
