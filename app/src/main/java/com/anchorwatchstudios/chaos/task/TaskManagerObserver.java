package com.anchorwatchstudios.chaos.task;

/**
 * Observes a <code>TaskManager</code> for progress.
 * 
 * @author Anthony Atella
 */
public interface TaskManagerObserver {

	/**
	 * Notify this observer that the given <code>TaskManager</code> has
	 * made progress.
	 * 
	 * @param taskManager The <code>TaskManager</code> that made progress
	 */
	void notify(TaskManager taskManager);
	
}
