package com.anchorwatchstudios.chaos.task;

import java.util.ArrayList;

/**
 * Manages one task at a time. If a new <code>task</code> is added the current
 * <code>task</code> is interrupted and removed, and the new one begins.
 * 
 * @author Anthony Atella
 */
public class SingleTaskManager implements TaskManager {

	private Task current;
	private ArrayList<TaskManagerObserver> observers;
	
	/**
	 * Constructs a new <code>SingleTaskManager</code>.
	 */
	public SingleTaskManager() {
		
		current = null;
		observers = new ArrayList<TaskManagerObserver>();
		
	}
	
	@Override
	public void notify(Task task) {

		notifyObservers();
		
		if(task.getProgress() == 1.0) {

			current = null;
			task.unregisterObserver(this);
			
		}
		
	}

	@Override
	public void addTask(Task task) {

		removeAll();
		current = task;
		current.registerObserver(this);
		new Thread(current).start();
		
	}

	@Override
	public void removeTask(Task task) {

		if(current.equals(task)) {
			
			current.stop();
			current.unregisterObserver(this);
			current = null;
			
		}
		
	}

	@Override
	public void removeAll() {

		if(current != null) {
		
			current.stop();
			current.unregisterObserver(this);
			current = null;
			
		}
		
	}

	@Override
	public Task getCurrent() {

		return current;

	}

	@Override
	public void registerObserver(TaskManagerObserver observer) {

		observers.add(observer);
		
	}

	@Override
	public void unregisterObserver(TaskManagerObserver observer) {

		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers() {

		for(int i = 0;i < observers.size();i++) {
			
			observers.get(i).notify(this);
			
		}
		
	}

}