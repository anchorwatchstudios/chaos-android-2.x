package com.anchorwatchstudios.chaos.task;

/**
 * A series of instructions to be completed over
 * some time.
 * 
 * @author Anthony Atella
 */
public interface Task extends Runnable {
	
	/**
	 * Pauses the task if possible.
	 */
	void togglePause();
	
	/**
	 * Returns true if the task is running.
	 * 
	 * @return true if the task is running.
	 */
	boolean isRunning();
	
	/**
	 * Returns true if the task is paused.
	 * 
	 * @return true if the task is paused.
	 */
	boolean isPaused();
	
	/**
	 * Cancels and stops the task as soon as possible.
	 */
	void stop();
	
	/**
	 * Registers an observer to be alerted when the task has made progress.
	 * 
	 * @param observer The <code>TaskObserver</code> to be
	 * registered
	 */
	void registerObserver(TaskObserver observer);
	
	/**
	 * Unregisters an observer.
	 * 
	 * @param observer The <code>TaskObserver</code> to be
	 * unregistered
	 */
	void unregisterObserver(TaskObserver observer);	
	
	/**
	 * Notifies all registered <code>TaskObservers</code>s that
	 * this <code>Task</code> has made progress.
	 */
	void notifyObservers();
	
	/**
	 * Returns the current task message.
	 * 
	 * @return The current task message
	 */
	String getMessage();
	
	/**
	 * Returns the current task progress.
	 * 
	 * @return The current task progress.
	 */
	double getProgress();
	
}
