package com.anchorwatchstudios.chaos.task;

/**
 * Abstractly manages tasks.
 * 
 * @author Anthony Atella
 */
public interface TaskManager extends TaskObserver {

	/**
	 * Adds a <code>Task</code>
	 * 
	 * @param task The <code>Task</code> to add
	 */
	void addTask(Task task);
	
	/**
	 * Removes a <code>Task</code>
	 * 
	 * @param task The <code>Task</code> to remove
	 */
	void removeTask(Task task);
	
	/**
	 * Removes all <code>Tasks</code>
	 */
	void removeAll();
	
	/**
	 * Returns the current <code>Task</code>
	 * 
	 * @return The current <code>Task</code>
	 */
	Task getCurrent();
	
	/**
	 * Registers a <code>TaskManagerObserver</code> to be updated when the <code>TaskManager</code>s
	 * <code>Task</code>s make progress.
	 * 
	 * @param observer The <code>TaskManagerObserver</code> to be registered
	 */
	void registerObserver(TaskManagerObserver observer);
	
	/**
	 * Unregisters a <code>TaskManagerObserver</code>.
	 * 
	 * @param observer The <code>TaskManagerObserver</code> to be unregistered
	 */
	void unregisterObserver(TaskManagerObserver observer);
	
	/**
	 * Notifies all <code>TaskManagerObservers</code> that a <code>Task</code> made progress.
	 */
	void notifyObservers();
	
}
