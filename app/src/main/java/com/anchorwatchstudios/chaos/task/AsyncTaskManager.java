package com.anchorwatchstudios.chaos.task;

import java.util.ArrayList;

/**
 * Manages <code>Task</code>s in an asynchronous way. This manager spawns
 * threads to handle <code>Task</code>s until the max thread limit is reached.
 * When a <code>Task</code> completes, a new one is executed from the queue until
 * no more are left.
 * 
 * @author Anthony Atella
 */
public class AsyncTaskManager extends PriorityQueueTaskManager {

	public static final int DEFAULT_MAX_THREADS = 3;
	private Task current;
	private int maxThreads;
	private ArrayList<Task> taskPool;

	/**
	 * Constructs a new <code>AsyncTaskManager</code>.
	 */
	public AsyncTaskManager() {

		current = null;
		maxThreads = DEFAULT_MAX_THREADS;
		taskPool = new ArrayList<Task>();

	}

	/**
	 * Returns the max number of concurrent threads.
	 * 
	 * @return The max number of concurrent threads
	 */
	public int getMaxThreads() {
		
		return maxThreads;
		
	}
	
	/**
	 * Sets the max number of concurrent threads.
	 * 
	 * @param maxThreads The max number of concurrent threads
	 */
	public void setMaxThreads(int maxThreads) {
		
		this.maxThreads = maxThreads;
		
	}
	
	@Override
	public void addTask(Task task) {

		super.addTask(task);
		spawnThread();

	}

	@Override
	public void removeTask(Task task) {

		super.removeTask(task);
		taskPool.remove(task);
		spawnThread();

	}

	@Override
	public void removeAll() {

		super.removeAll();
		current = null;
		taskPool = new ArrayList<Task>();
		
	}

	@Override
	public Task getCurrent() {

		return current;

	}

	@Override
	public void notify(Task task) {

		notifyObservers();

		if(task.getProgress() == 1.0) {
			
			taskPool.remove(task);
			spawnThread();
			
		}

	}

	/**
	 * Spawns a new thread to execute a <code>Task</code>.
	 */
	private synchronized void spawnThread() {

		if(taskPool.size() == 0) {
			
			current = null;
			
		}
		
		if(taskPool.size() < maxThreads && tasks.size() > 0) {

			Task task = tasks.remove();
			taskPool.add(task);
			new Thread(task).start();

			if(current != null) {

				current = PriorityTask.compare(current, task);

			}
			else {

				current = task;

			}

		}

	}

}