package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Interpolation;

/**
 * A fractal tree. Every pair of branches grows recursively from
 * the last until the threshold of branch length is reached.
 * 
 * @author Anthony Atella
 */
public class Tree2D implements Fractal {

	public static final double DEFAULT_LENGTH = 100;
	public static final double DEFAULT_ANGLE = Math.PI / 4.0;
	public static final double DEFAULT_THRESHOLD = 1;
	public static final double DEFAULT_STEP = 0.6;
	private double length;
	private double angle;
	private double threshold;
	private double step;
	
	/**
	 * Constructs a new <code>Tree</code> with default values.
	 */
	public Tree2D() {
	
		length = DEFAULT_LENGTH;
		angle = DEFAULT_ANGLE;
		threshold = DEFAULT_THRESHOLD;
		step = DEFAULT_STEP;
		
	}
	
	/**
	 * Constructs a new <code>Tree2D</code> with the given values.
	 * 
	 * @param length The initial length of the trunk
	 * @param angle The angle at which to branch
	 * @param threshold The threshold
	 * @param step The scalar value that length is multiplied by each iteration
	 */
	public Tree2D(double length, double angle, double threshold, double step) {
		
		this();
		this.length = length;
		this.angle = angle;
		this.threshold = threshold;
		this.step = step;
		
	}

	/**
	 * Returns the initial branch length.
	 * 
	 * @return The initial branch length
	 */
	public double getLength() {
		
		return length;
		
	}
	
	/**
	 * Sets the initial branch length.
	 * 
	 * @param branchLength The initial branch length
	 */
	public void setLength(double branchLength) {
		
		this.length = branchLength;
		
	}
	
	/**
	 * Returns the angle that the branches will turn at
	 * on each iteration.
	 * 
	 * @return The angle that the branches will turn at
	 * on each iteration
	 */
	public double getAngle() {
		
		return angle;
		
	}
	
	/**
	 * Sets the angle that the branches will turn at
	 * on each iteration.
	 * 
	 * @param angle The angle that the branches will turn at
	 * on each iteration
	 */
	public void setAngle(double angle) {
		
		this.angle = angle;
		
	}
	
	/**
	 * Returns the threshold minimum length.
	 * 
	 * @return The threshold minimum length
	 */
	public double getThreshold() {
		
		return threshold;
		
	}
	
	/**
	 * Sets the threshold minimum length.
	 * 
	 * @param threshold The threshold minimum length
	 */
	public void setThreshold(double threshold) {
		
		this.threshold = threshold;
		
	}

	/**
	 * Returns the amount to reduce the length by,
	 * in percent, for each step.
	 * 
	 * @return The amount to reduce the length by,
	 * in percent, for each step
	 */
	public double getStep() {

		return step;
		
	}
	
	/**
	 * Sets the amount to reduce the length by,
	 * in percent, for each step.
	 * 
	 * @param step The amount to reduce the length by,
	 * in percent, for each step
	 */
	public void setStep(double step) {
		
		this.step = step;
		
	}

	@Override
	public String toString() {
		
		return Strings.FRACTAL_TREE;
		
	}

	@Override
	public String getDescription() {

		String result = "Tree: \n";
		result += "  Length: " + length + "\n";
		result += "  Angle: " + angle + "\n";
		result += "  Threshold: " + threshold + "\n";
		result += "  Step: " + step + "\n\n";
		return result;

	}
	
	@Override
	public Tree2D clone() {
		
		return new Tree2D(length, angle, threshold, step);
		
	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {
		
		if(fractal instanceof Tree2D) {
			
			Tree2D tree2D = (Tree2D) fractal;
			Tree2D result = new Tree2D();
			result.setLength(Interpolation.interpolate(length, tree2D.getLength(), percent));
			result.setAngle(Interpolation.interpolate(angle, tree2D.getAngle(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, tree2D.getThreshold(), percent));
			result.setStep(Interpolation.interpolate(step, tree2D.getStep(), percent));
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}
		
	}
	
}
