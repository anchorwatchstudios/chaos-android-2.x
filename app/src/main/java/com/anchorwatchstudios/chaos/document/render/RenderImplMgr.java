package com.anchorwatchstudios.chaos.document.render;

import com.anchorwatchstudios.chaos.document.fractal.Fractal;

public interface RenderImplMgr<T> {

	void registerImpl(Class<? extends Fractal> fractalClass, Class<? extends T> object);
	T getImpl(Class<? extends Fractal> fractalClass);
	
}
