package com.anchorwatchstudios.chaos.document.shader;

import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * A <code>Shader</code> takes a value between 0 and 1
 * and returns a color. If the shader is inverted the value
 * becomes (1.0 - value).
 * 
 * @author Anthony Atella
 *
 */
public interface Shader extends Interpolation<Shader>, Cloneable {

	/**
	 * Returns the text description of this <code>Shader</code> and
	 * its components.
	 * 
	 * @return The text description of this <code>Shader</code> and its components
	 */
	String getDescription();
	
	/**
	 * Returns a color from the given value.
	 * 
	 * @param value A 4d vector: vec4d(renderIterations, distance, minDistance, magnitude)
	 * @return A color from the given value
	 */
	Color getFragment(Vec4d value);
	
	/**
	 * Returns true if the shader is inverted.
	 * 
	 * @return True if the shader is inverted
	 */
	boolean isInverted();
	
	/**
	 * Sets whether or not the shader is inverted.
	 * 
	 * @param inverted Whether or not the shader is inverted
	 */
	void setInverted(boolean inverted);
	
	/**
	 * Returns a clone of this Shader.
	 * 
	 * @return A clone of this Shader
	 */
	Shader clone();
	
}
