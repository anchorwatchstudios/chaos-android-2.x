package com.anchorwatchstudios.chaos.document;

/**
 * The primary file type for Chaos2. This interface holds
 * all the essential information needed to make a Chaos2
 * .frc2 file.
 * 
 * @author Anthony Atella
 */
public interface FRC2Document extends Document {

	/**
	 * Adds a <code>KeyFrame</code>.
	 * 
	 * @param keyFrame The <code>KeyFrame</code> to be added
	 */
	void addKeyFrame(KeyFrame keyFrame);
	
	/**
	 * Removes a <code>KeyFrame</code> at the given index.
	 * 
	 * @param index The index of the <code>KeyFrame</code>
	 */
	void removeKeyFrame(int index);
	
	/**
	 * Removes the current <code>KeyFrame</code>.
	 */
	void removeCurrentKeyFrame();
	
	/**
	 * Returns the <code>KeyFrame</code> at the given index.
	 * 
	 * @param index The index
	 * @return The <code>KeyFrame</code> at the given index, 
	 *         or null if one is not found at the given index
	 */
	KeyFrame getKeyFrame(int index);
	
	/**
	 * Returns the current <code>KeyFrame</code>.
	 * 
	 * @return the current <code>KeyFrame</code>.
	 */
	KeyFrame getCurrentKeyFrame();
	
	/**
	 *  Returns the current <code>KeyFrame</code> index.
	 *  
	 * @return  The current <code>KeyFrame</code> index
	 */
	int getKeyFrameIndex();
	
	/**
	 * Sets the current <code>KeyFrame</code> index to the given index.
	 * 
	 * @param index The new index
	 */
	void setKeyFrameIndex(int index);
	
	/**
	 * Returns the number of <code>KeyFrame</code>s.
	 * 
	 * @return The number of <code>KeyFrame</code>s
	 */
	int getKeyFrameCount();
	
	/**
	 * Returns this <code>FRC2Document</code>s <code>Settings</code>.
	 * 
	 * @return This <code>FRC2Document</code>s <code>Settings</code>
	 */
	Settings getSettings();
	
	/**
	 * Sets this <code>FRC2Document</code>s <code>Settings</code>.
	 * 
	 * @param settings The new settings
	 */
	void setSettings(Settings settings);
	
}
