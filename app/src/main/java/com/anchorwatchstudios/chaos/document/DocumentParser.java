package com.anchorwatchstudios.chaos.document;

import java.io.File;

/**
 * This interface is responsible for loading and saving
 * <code>Document</code>s.
 * 
 * @author Anthony Atella
 */
public interface DocumentParser {

	/**
	 * Loads the given <code>Document</code>.
	 * 
	 * @param file The file to be loaded
	 * @return The document if loaded successfully, null otherwise
	 */
	Document load(File file);
	
	/**
	 * Saves the given <code>Document</code>.
	 * 
	 * @param document The <code>Document</code>
	 * @return true if the operation was successful
	 */
	boolean save(Document document);
	
}
