package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Discrete;
import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * Represents the Mandelbrot set. Contains all the information used
 * to calculate it.
 * 
 * @author Anthony Atella
 */
public class Mandelbrot implements EscapeTimeFractal<Complex>, Discrete {

	public static final Complex DEFAULT_Z = new Complex(0, 0);
	private Complex z;
	private int iterations;
	private double threshold;
	private boolean discrete;

	/**
	 * Constructs a new <code>Mandelbrot</code> with default values.
	 */
	public Mandelbrot() {

		z = DEFAULT_Z;
		iterations = EscapeTimeFractal.DEFAULT_ITERATIONS;
		threshold = EscapeTimeFractal.DEFAULT_THRESHOLD;
		discrete = Discrete.DEFAULT;

	}

	/**
	 * Constructs a new <code>Mandelbrot</code> with the given values.
	 * 
	 * @param z The complex number 'z0', from zn = zn-1 + C
	 * @param iterations The number of iterations
	 * @param threshold The threshold
	 * @param discrete Whether or not this fractal is calculated discretely
	 */
	public Mandelbrot(Complex z, int iterations, double threshold, boolean discrete) {

		this.z = z;
		this.iterations = iterations;
		this.threshold = threshold;
		this.discrete = discrete;

	}

	/**
	 * Returns the complex number 'z0', from zn = zn-1 + C.
	 * 
	 * @return The complex number 'z0', from zn = zn-1 + C
	 */
	public Complex getZ() {

		return z;

	}

	/**
	 * Sets the complex number 'z0', from zn = zn-1 + C.
	 * 
	 * @param z The complex number 'z0', from zn = zn-1 + C
	 */
	public void setZ(Complex z) {

		this.z = z;

	}
	
	@Override
	public Vec4d isInSet(Complex point) {

		double result = 0;
		Complex zCopy = new Complex(z.getA(), z.getB());
		double minDistance = 1000.0;
		
		for(int i = 0;i < iterations;i++) {

			zCopy = zCopy.multiply(zCopy).add(point);
			double l = zCopy.abs();
			minDistance = Math.min(minDistance, l);
			
			if(l >= Math.pow(threshold, 2.0)) {

				break;

			}

			result++;

		}


		if(!discrete) {

			result = EscapeTimeFractal.getPreciseIterations(zCopy, result, iterations);
			
		}
		
		double length = z.abs();
		return new Vec4d(result / (double) iterations, length, minDistance, 0.33 * Math.log(length * length) + 1.0);
		
	}

	@Override
	public int getIterations() {

		return iterations;

	}

	@Override
	public void setIterations(int iterations) {

		this.iterations = iterations;

	}

	@Override
	public double getThreshold() {

		return threshold;

	}

	@Override
	public void setThreshold(double threshold) {

		this.threshold = threshold;

	}

	@Override
	public boolean getDiscrete() {

		return discrete;

	}

	@Override
	public void setDiscrete(boolean discrete) {

		this.discrete = discrete;

	}
	
	@Override
	public String toString() {
		
		return Strings.FRACTAL_MANDELBROT;
		
	}

	@Override
	public String getDescription() {

		String result = "Mandelbrot: \n";
		result += "  Z: " + z + "\n";
		result += "  Iterations: " + iterations + "\n";
		result += "  Threshold: " + threshold + "\n";
		result += "  Discrete: " + discrete + "\n\n";
		return result;
	
	}
	
	@Override
	public Mandelbrot clone() {
		
		return new Mandelbrot(new Complex(z.getA(), z.getB()), iterations, threshold, discrete);
		
	}
	
	@Override
	public Class<Complex> getType() {

		return Complex.class;

	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {

		if(fractal instanceof Mandelbrot) {
			
			Mandelbrot mandelbrot = (Mandelbrot) fractal;
			Mandelbrot result = new Mandelbrot();
			result.setZ(z.interpolate(mandelbrot.getZ(), percent));
			result.setIterations(Interpolation.interpolate(iterations, mandelbrot.getIterations(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, mandelbrot.getThreshold(), percent));
			
			if(percent < 0.5) {
				
				result.setDiscrete(discrete);
				
			}
			else {
				
				result.setDiscrete(mandelbrot.getDiscrete());
				
			}
			
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}
		
	}
	
}