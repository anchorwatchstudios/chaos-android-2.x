package com.anchorwatchstudios.chaos.document.render;

import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Vec3d;

/**
 * Represents the settings needed to implement a raymarcher
 * with a look-at camera.
 * 
 * @author Anthony Atella
 */
public class RayMarch implements RenderMethod {

	private static final int DEFAULT_ITERATIONS = 10;
	private static final double DEFAULT_RAY_THRESHOLD = 0.005;
	private static final Vec3d DEFAULT_CAMERA_ORIGIN = new Vec3d(0.0, 0.0, 2.0);
	private static final Vec3d DEFAULT_CAMERA_LOOKAT = new Vec3d(0.0, 0.0, -1.0);
	private static final Vec3d DEFAULT_CAMERA_UP = new Vec3d(0.0, 1.0, 0.0);
	private int iterations;
	private double rayThreshold;
	private Vec3d cameraOrigin;
	private Vec3d cameraLookAt;
	private Vec3d cameraUp;
	
	/**
	 * Constructs a new <code>RayMarch</code> with default values.
	 */
	public RayMarch() {
		
		iterations = DEFAULT_ITERATIONS;
		rayThreshold = DEFAULT_RAY_THRESHOLD;
		setCameraOrigin(DEFAULT_CAMERA_ORIGIN);
		setCameraLookAt(DEFAULT_CAMERA_LOOKAT);
		setCameraUp(DEFAULT_CAMERA_UP);
		
	}
	
	/**
	 * Constructs a new <code>RayMarch</code> with the given values.
	 *
	 * @param iterations The max number of times to march
	 * @param rayThreshold The minimum distance a ray must be from the geometry before it is considered touching
	 * @param cameraOrigin The camera origin
	 * @param cameraLookAt The camera look-at vector
	 * @param cameraUp The camera up vector
	 */
	public RayMarch(int iterations, double rayThreshold, Vec3d cameraOrigin, Vec3d cameraLookAt, Vec3d cameraUp) {
		
		this.iterations = iterations;
		this.rayThreshold = rayThreshold;
		this.cameraOrigin = cameraOrigin;
		this.cameraLookAt = cameraLookAt;
		this.cameraUp = cameraUp;
		
	}
	
	/**
	 * Returns the max number of times the ray marcher will march.
	 * 
	 * @return The max number of times the ray marcher will march.
	 */
	public int getIterations() {
		
		return iterations;
		
	}
	
	/**
	 * Sets the max number of times the ray marcher will march.
	 * 
	 * @param iterations The max number of times the ray marcher will march.
	 */
	public void setIterations(int iterations) {
		
		this.iterations = iterations;
		
	}
	
	/**
	 * Returns the minimum distance a ray must be from the geometry 
	 * before it is considered touching.
	 * 
	 * @return The minimum distance a ray must be from the geometry before it is considered touching
	 */
	public double getRayThreshold() {
		
		return rayThreshold;
		
	}
	
	/**
	 * Sets the minimum distance a ray must be from the geometry 
	 * before it is considered touching.
	 * 
	 * @param rayThreshold The minimum distance a ray must be from the geometry before it is considered touching.
	 */
	public void setRayThreshold(double rayThreshold) {
		
		this.rayThreshold = rayThreshold;
		
	}

	/**
	 * Returns the camera origin.
	 * 
	 * @return The camera origin
	 */
	public Vec3d getCameraOrigin() {
		
		return cameraOrigin;
		
	}

	/**
	 * Sets the camera origin to the given origin.
	 * 
	 * @param cameraOrigin The new camera origin
	 */
	public void setCameraOrigin(Vec3d cameraOrigin) {
		
		this.cameraOrigin = cameraOrigin;
		
	}

	/**
	 * Returns the camera look-at vector.
	 * 
	 * @return The camera look-at vector
	 */
	public Vec3d getCameraLookAt() {
		
		return cameraLookAt;
		
	}

	/**
	 * Sets the camera look-at vector to the given <code>Vec3d</code>.
	 * 
	 * @param cameraLookAt The new camera look-at vector
	 */
	public void setCameraLookAt(Vec3d cameraLookAt) {
		
		this.cameraLookAt = cameraLookAt;
		
	}

	/**
	 * Returns the camera up vector.
	 * 
	 * @return The camera up vector
	 */
	public Vec3d getCameraUp() {
		
		return cameraUp;
		
	}

	/**
	 * Sets the camera up vector to the given <code>Vec3d</code>.
	 * 
	 * @param cameraUp The new camera up vector
	 */
	public void setCameraUp(Vec3d cameraUp) {
		
		this.cameraUp = cameraUp;
		
	}

	@Override
	public RenderImplementation[] getCapabilities() {

		return new RenderImplementation[] { RenderImplementation.OPENGL4 };
		
	}
	
	@Override
	public String getDescription() {

		return "RayMarch Settings: \n" +
				"  Iterations: " + iterations + "\n" +
				"  Threshold: " + rayThreshold + "\n" + 
				"  Camera Origin: " + cameraOrigin + "\n" + 
				"  Camera Look-At: " + cameraLookAt + "\n" +
				"  Camera Up: " + cameraUp + "\n\n";
		
	}

	@Override
	public RayMarch clone() {

		return new RayMarch(iterations, rayThreshold, cameraOrigin.clone(), cameraLookAt.clone(), cameraUp.clone());
		
	}

	@Override
	public RenderMethod interpolate(RenderMethod item, double percent) {
		
		if(item instanceof RayMarch) {
		
			RayMarch rayMarchSettings = (RayMarch) item;
			RayMarch result = new RayMarch();
			result.setCameraLookAt(cameraLookAt.interpolate(rayMarchSettings.getCameraLookAt(), percent));
			result.setCameraUp(cameraUp.interpolate(rayMarchSettings.getCameraUp(), percent));
			result.setCameraOrigin(cameraOrigin.interpolate(rayMarchSettings.getCameraOrigin(), percent));
			result.setIterations(Interpolation.interpolate(iterations, rayMarchSettings.getIterations(), percent));
			result.setRayThreshold(Interpolation.interpolate(rayThreshold, rayMarchSettings.getRayThreshold(), percent));
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return item.clone();
			
		}

	}
	
}
