package com.anchorwatchstudios.chaos.document;

import java.util.ArrayList;

/**
 * An implementation of the <code>DocumentManager</code> interface that uses
 * an <code>ArrayList</code> to store its <code>
 * DocumentManagerObserver</code>s and <code>Document</code>s.
 * 
 * @author Anthony Atella
 */
public class ArrayListDocumentManager implements DocumentManager {

	private ArrayList<Document> document;
	private ArrayList<DocumentManagerObserver> observer;
	private int index;
	
	/**
	 * Creates a new <code>ArrayListDocumentManager</code> with
	 * an empty list of observers and documents and an index of -1. 
	 */
	public ArrayListDocumentManager() {

		document = new ArrayList<Document>();
		observer = new ArrayList<DocumentManagerObserver>();
		index = -1;

	}
	
	/**
	 * Returns the number of registered <code>DocumentManagerObserver</code>s.
	 * 
	 * @return The number of registered <code>DocumentManagerObserver</code>s
	 */
	public int getObserverCount() {

		return observer.size();
		
	}
	
	@Override
	public int getDocumentCount() {

		return document.size();
		
	}
	
	@Override
	public void notify(Document document) {

		notifyObservers();
		
	}

	@Override
	public void add(Document document) {

		document.registerObserver(this);
		this.document.add(document);
		index = this.document.size() - 1;
		notifyObservers();
		
	}

	@Override
	public void remove(int index) {
		
		if(index <= document.size() && index > -1) {

			document.remove(index).unregisterObserver(this);
			
			if(this.index >= index) {
				
				this.index--;
				
			}
			
			notifyObservers();
			
		}
		
	}

	@Override
	public int getIndex() {

		return index;

	}

	@Override
	public void setIndex(int index) {

		this.index = index;
		notifyObservers();
		
	}

	@Override
	public Document getCurrent() {

		if(index == -1) {
			
			return null;
			
		}
		
		return document.get(index);

	}

	@Override
	public boolean hasDocument(Document document) {

		return this.document.contains(document);

	}

	@Override
	public void registerObserver(DocumentManagerObserver observer) {

		this.observer.add(observer);
		
	}

	@Override
	public void unregisterObserver(DocumentManagerObserver observer) {

		this.observer.remove(observer);
		
	}

	@Override
	public void notifyObservers() {

		for(int i = 0;i < observer.size();i++) {
			
			observer.get(i).notify(this);
			
		}
		
	}

	@Override
	public String toString() {
		
		String result = "ArrayListDocumentManager {\n";
		result += "  Documents:\n";
		
		for(int i = 0;i < document.size();i++) {
			
			result += "    " + i + ": " + document.get(i).toString() + "\n";
			
		}
		
		result += "  Observers:\n";
		
		for(int i = 0;i < observer.size();i++) {
			
			result += "    " + i + ": " + observer.get(i).toString() + "\n";
		
		}

		result += "  Index: " + index + "\n";
		result += "}";
		return result;
		
	}
	
}
