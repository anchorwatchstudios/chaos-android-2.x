package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.Strings;
import com.anchorwatchstudios.chaos.math.Interpolation;
import com.anchorwatchstudios.chaos.math.Triplex;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * Represents the Mandelbox set in triplex space. Contains 
 * all the information used to calculate it.
 * 
 * @author Anthony Atella
 */
public class Mandelbox implements EscapeTimeFractal<Triplex> {

	private static final double DEFAULT_THRESH =  4.0;
	private static final double DEFAULT_FOLD_LIMIT = 1.0;
	private static final double DEFAULT_MIN_RADIUS = 0.5;
	private static final double DEFAULT_FIXED_RADIUS =  1.0;
	private static final double DEFAULT_SCALE = 2.0;
	private int iterations;
	private double threshold;
	private double foldLimit;
	private double minRadius;
	private double fixedRadius;
	private double scale;
	
	/**
	 * Constructs a new Mandelbox with default values.
	 */
	public Mandelbox() {

		iterations = EscapeTimeFractal.DEFAULT_ITERATIONS;
		threshold = Mandelbox.DEFAULT_THRESH;
		foldLimit = DEFAULT_FOLD_LIMIT;
		minRadius = DEFAULT_MIN_RADIUS;
		fixedRadius = DEFAULT_FIXED_RADIUS;
		scale = DEFAULT_SCALE;
		
	}
	
	/**
	 * Constructs a new Mandelbox with the given values.
	 * 
	 * @param iterations The number of fractal iterations
	 * @param threshold The threshold radius
	 * @param foldLimit The amount to fold
	 * @param minRadius The min fold radius
	 * @param fixedRadius The fixed fold radius
	 * @param scale The scale of the box
	 */
	public Mandelbox(int iterations, double threshold, double foldLimit, double minRadius, double fixedRadius, double scale) {

		this.iterations = iterations;
		this.threshold = threshold;
		this.foldLimit = foldLimit;
		this.minRadius = minRadius;
		this.fixedRadius = fixedRadius;
		this.scale = scale;
		
	}

	/**
	 * Returns the fold limit of this Mandelbox.
	 * 
	 * @return The fold limit
	 */
	public double getFoldLimit() {
		
		return foldLimit;
		
	}

	/**
	 * Sets the fold limit for this Mandelbox.
	 * 
	 * @param foldLimit The new fold limit
	 */
	public void setFoldLimit(double foldLimit) {
		
		this.foldLimit = foldLimit;
		
	}

	/**
	 * Returns the min. radius of this Mandelbox.
	 * 
	 * @return The min. radius
	 */
	public double getMinRadius() {
		
		return minRadius;
		
	}

	/**
	 * Sets the min. radius for this Mandelbox.
	 * 
	 * @param minRadius The new min. radius
	 */
	public void setMinRadius(double minRadius) {
		
		this.minRadius = minRadius;
		
	}

	/**
	 * Returns the fixed radius of this Mandelbox.
	 * 
	 * @return The fixed radius
	 */
	public double getFixedRadius() {
		
		return fixedRadius;
		
	}

	/**
	 * Sets the fixed radius for this Mandelbox.
	 * 
	 * @param fixedRadius The new fixed radius
	 */
	public void setFixedRadius(double fixedRadius) {
		
		this.fixedRadius = fixedRadius;
		
	}

	/**
	 * Returns the scale of this Mandelbox.
	 * 
	 * @return The scale of this Mandelbox
	 */
	public double getScale() {
		
		return scale;
		
	}

	/**
	 * Sets the scale of this Mandelbox.
	 * 
	 * @param scale The new scale
	 */
	public void setScale(double scale) {
		
		this.scale = scale;
		
	}

	@Override
	public String getDescription() {

		return "";

	}

	@Override
	public Mandelbox clone() {

		return new Mandelbox(iterations, threshold, foldLimit, minRadius, fixedRadius, scale);
		
	}

	@Override
	public Fractal interpolate(Fractal fractal, double percent) {

		if(fractal instanceof Mandelbox) {
			
			Mandelbox mandelbox = (Mandelbox) fractal;
			Mandelbox result = new Mandelbox();
			result.setIterations(Interpolation.interpolate(iterations, mandelbox.getIterations(), percent));
			result.setThreshold(Interpolation.interpolate(threshold, mandelbox.getThreshold(), percent));
			result.setFoldLimit(Interpolation.interpolate(foldLimit, mandelbox.getFoldLimit(), percent));
			result.setMinRadius(Interpolation.interpolate(minRadius, mandelbox.getMinRadius(), percent));
			result.setFixedRadius(Interpolation.interpolate(fixedRadius, mandelbox.getFixedRadius(), percent));
			result.setScale(Interpolation.interpolate(scale, mandelbox.getScale(), percent));
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return fractal.clone();
			
		}

	}

	@Override
	public Vec4d isInSet(Triplex point) {

		// Unimplemented
		return new Vec4d();
		
	}

	@Override
	public int getIterations() {
		
		return iterations;
		
	}

	@Override
	public void setIterations(int iterations) {

		this.iterations = iterations;
		
	}

	@Override
	public double getThreshold() {

		return threshold;
		
	}

	@Override
	public void setThreshold(double threshold) {

		this.threshold = threshold;
		
	}

	@Override
	public Class<Triplex> getType() {

		return Triplex.class;

	}

	@Override
	public String toString() {
		
		return Strings.FRACTAL_MANDELBOX;
	}
	
}
