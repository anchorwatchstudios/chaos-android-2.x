package com.anchorwatchstudios.chaos.document;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.reflect.ReflectionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;

/**
 * An implementation of <code>DocumentParser</code> that uses
 * Google's JSON library, GSON to read and write to .frc2 format.
 * 
 * @author Anthony Atella
 */
public class GSONFRC2DocumentParser implements DocumentParser {

	private Gson gson;
	
	/**
	 * Constructs a new <code>GSONDocumentParser</code>.
	 */
	public GSONFRC2DocumentParser() {
		
		// Register fractal types
		RuntimeTypeAdapterFactory<Fractal> fractalFactory = RuntimeTypeAdapterFactory.of(Fractal.class, "type");
		@SuppressWarnings("unchecked")
		Class<? extends Fractal>[] clazz0 = (Class<? extends Fractal>[]) ReflectionUtils.getSubClasses("com.anchorwatchstudios.chaos2.document.fractal", Fractal.class);
		
		for(int i = 0;i < clazz0.length;i++) {
		
			fractalFactory.registerSubtype(clazz0[i], clazz0[i].getSimpleName());
			
		}
		
		// Register shader types
		RuntimeTypeAdapterFactory<Shader> shaderFactory = RuntimeTypeAdapterFactory.of(Shader.class, "type");
		@SuppressWarnings("unchecked")
		Class<? extends Shader>[] clazz1 = (Class<? extends Shader>[]) ReflectionUtils.getSubClasses("com.anchorwatchstudios.chaos2.document.shader", Shader.class);
		
		for(int i = 0;i < clazz1.length;i++) {
		
			shaderFactory.registerSubtype(clazz1[i], clazz1[i].getSimpleName());
			
		}
		
		// Register render types
		RuntimeTypeAdapterFactory<RenderMethod> renderFactory = RuntimeTypeAdapterFactory.of(RenderMethod.class, "type");
		@SuppressWarnings("unchecked")
		Class<? extends RenderMethod>[] clazz2 = (Class<? extends RenderMethod>[]) ReflectionUtils.getSubClasses("com.anchorwatchstudios.chaos2.document.render", RenderMethod.class);
		
		for(int i = 0;i < clazz2.length;i++) {
		
			renderFactory.registerSubtype(clazz2[i], clazz2[i].getSimpleName());
			
		}
		
		gson = new GsonBuilder()
				//.setPrettyPrinting()
				.registerTypeAdapterFactory(fractalFactory)
				.registerTypeAdapterFactory(shaderFactory)
				.registerTypeAdapterFactory(renderFactory)
				.create();
		
	}

	@Override
	public FRC2Document load(File file) {

		FRC2Document result = null;
		
		try {
			
			FileInputStream fis = new FileInputStream(file);
	        BufferedReader br;
	        br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
			result = gson.fromJson(br, ArrayListFRC2Document.class);
			result.setFile(file);
			br.close();
			
		}
		catch (Exception e) {
		
			e.printStackTrace();
			
		}
		
		return result;
		
	}

	@Override
	public boolean save(Document document) {
		
		try {
			
	        FileOutputStream outputStream = new FileOutputStream(document.getFile());
	        BufferedWriter bufferedWriter;
	        bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
			gson.toJson(document, bufferedWriter);
			bufferedWriter.close();
			
		}
		catch (Exception e) {
		
			e.printStackTrace();
			return false;
			
		}
		
		return true;
		
	}
	
}
