package com.anchorwatchstudios.chaos.document;

/**
 * Observes a <code>Document</code> and is notified by the
 * <code>Document</code> when the <code>Document</code> is edited.
 * Utilizes the observer pattern.
 * 
 * @author Anthony Atella
 */
public interface DocumentObserver {

	/**
	 * Notifies this <code>DocumentObserver</code> that
	 * the <code>Document</code> has been edited.
	 * 
	 * @param document The <code>Document</code> that 
	 * has been edited.
	 */
	void notify(Document document);
	
}
