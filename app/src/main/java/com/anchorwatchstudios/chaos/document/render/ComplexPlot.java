package com.anchorwatchstudios.chaos.document.render;

import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Interpolation;

/**
 * Represents a 2D complex plot. Contains minimum and
 * maximum complex numbers, a rotation in radians,
 * and methods to pan, rotate, and zoom.
 * 
 * @author Anthony Atella
 */
public class ComplexPlot implements RenderMethod {

	private static final double DEFAULT_RADIUS = 1.5;
	private Complex min;
	private Complex max;
	private double rotation;
	
	/**
	 * Constructs a new <code>ComplexPlot</code> with default values.
	 */
	public ComplexPlot() {

		min = new Complex(-DEFAULT_RADIUS, -DEFAULT_RADIUS);
		max = new Complex(DEFAULT_RADIUS, DEFAULT_RADIUS);
		rotation = 0.0;

	}

	/**
	 * Constructs a new <code>ComplexPlot</code> with the given values.
	 * 
	 * @param min The minimum point on the graph
	 * @param max The maximum pont on the graph
	 * @param rotation The rotation, in radians
	 */
	public ComplexPlot(Complex min, Complex max, double rotation) {
		
		this.min = min;
		this.max = max;
		this.rotation = rotation;
		
	}
	
	/**
	 * Returns the minimum point on the graph.
	 * 
	 * @return The minimum point on the graph
	 */
	public Complex getMin() {
		
		return min;
		
	}

	/**
	 * Sets the minimum point on the graph.
	 * 
	 * @param min The new minimum point on the graph
	 */
	public void setMin(Complex min) {
		
		this.min = min;
		
	}

	/**
	 * Returns the maximum point on the graph.
	 * 
	 * @return The maximum point on the graph
	 */
	public Complex getMax() {
		
		return max;
		
	}

	/**
	 * Sets the maximum point on the graph.
	 * 
	 * @param max The new maximum point on the graph
	 */
	public void setMax(Complex max) {
		
		this.max = max;
		
	}

	/**
	 * Returns the rotation of the camera.
	 * 
	 * @return The rotation of the camera
	 */
	public double getRotation() {

		return rotation;
		
	}
	
	/**
	 * Sets the rotation of the camera.
	 * 
	 * @param rotation The new camera rotation
	 */
	public void setRotation(double rotation) {
		
		this.rotation = rotation;
		
	}
	
	/**
	 * Pans the graph according to the given values.
	 * Values should be in normalized graph coordinates.
	 * 
	 * @param x The x translation in normalized graph coordinates
	 * @param y The y translation in normalized graph coordinates
	 */
	public void pan(double x, double y) {
		
		double xInterval = Math.abs(max.getA() - min.getA());
		double yInterval = Math.abs(max.getB() - min.getB());
		Complex point = new Complex(x * xInterval, y * yInterval);
		setMin(min.add(point));
		setMax(max.add(point));
		
	}
	
	/**
	 * Zooms the graph according to the given value.
	 * The value should be in normalized screen coordinates.
	 * 
	 * @param amount The amount to zoom in normalized screen coordinates
	 */
	public void zoom(double amount) {
		
		double xInterval = Math.abs(max.getA() - min.getA());
		double yInterval = Math.abs(max.getB() - min.getB());
		double minX = min.getA() - (xInterval * amount / 2);
		double maxX = max.getA() + (xInterval * amount / 2);
		double minY = min.getB() - (yInterval * amount / 2);
		double maxY = max.getB() + (yInterval * amount / 2);
		setMin(new Complex(minX, minY));
		setMax(new Complex(maxX, maxY));
		
	}


	@Override
	public RenderImplementation[] getCapabilities() {

		return new RenderImplementation[] { RenderImplementation.OPENGL4, RenderImplementation.JAVA2D };
		
	}
	
	@Override
	public String getDescription() {

		return "Complex Plot Settings: \n" +
				"  Min: " + min.toString() + "\n" +
				"  Max: " + max.toString() + "\n" +
				"  Rotation: " + rotation + " degrees\n\n";
	}

	@Override
	public ComplexPlot clone() {

		return new ComplexPlot(new Complex(min.getA(), min.getB()), new Complex(max.getA(), max.getB()), rotation);
		
	}

	@Override
	public RenderMethod interpolate(RenderMethod item, double percent) {

		if(item instanceof ComplexPlot) {

			ComplexPlot complexPlotSettings = (ComplexPlot) item;
			ComplexPlot result = new ComplexPlot();
			result.setMin(min.interpolate(complexPlotSettings.getMin(), percent));
			result.setMax(max.interpolate(complexPlotSettings.getMax(), percent));
			result.setRotation(Interpolation.interpolate(rotation, complexPlotSettings.getRotation(), percent));
			return result;
			
		}
		else if(percent < 0.5) {
			
			return this.clone();
			
		}
		else {
			
			return item.clone();
			
		}
		
	}
	
}
