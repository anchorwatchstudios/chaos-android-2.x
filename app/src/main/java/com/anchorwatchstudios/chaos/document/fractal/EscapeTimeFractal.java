package com.anchorwatchstudios.chaos.document.fractal;

import com.anchorwatchstudios.chaos.math.Complex;
import com.anchorwatchstudios.chaos.math.Vec4d;

/**
 * A common interface for all fractals that use the
 * escape time method of calculation, like the Julia
 * and Mandelbrot.
 * 
 * @author Anthony Atella
 *
 * @param <T> The number system used to calculate
 * this fractal
 */
public interface EscapeTimeFractal<T> extends Fractal {

	static final int DEFAULT_ITERATIONS = 25;
	static final double DEFAULT_THRESHOLD = 2.0;
	
	/**
	 * Returns whether or not the point is in
	 * the set.
	 * 
	 * @param point A point of type T
	 * @return A vec4d: vec4d(iterations, distance, minDistance, magnitude)
	 */
	Vec4d isInSet(T point);
	
	/**
	 * Returns the number of iterations.
	 * 
	 * @return The number of iterations
	 */
	int getIterations();
	
	/**
	 * Sets the number of iterations the given
	 * value.
	 * 
	 * @param iterations The number of iterations
	 */
	void setIterations(int iterations);
	
	/**
	 * Returns the threshold.
	 * 
	 * @return The threshold
	 */
	double getThreshold();
	
	/**
	 * Sets the threshold to the given value.
	 * 
	 * @param threshold The threshold
	 */
	void setThreshold(double threshold);
	
	/**
	 * Returns an estimation of the continuous amount of iterations
	 * it took to approach infinity.
	 * 
	 * @param z The final value of z after running the algorithm
	 * @param iterations The number of iterations it took to break the threshold
	 * @param maxIterations The maximum number of iterations
	 * @return The precise decimal number of iterations it took to break the threshold
	 */
	static double getPreciseIterations(Complex z, double iterations, double maxIterations) {

	    double log_zn = Math.log(z.abs()) / 2;
	    double nu = Math.log(log_zn / Math.log(2)) / Math.log(2);
	    double result = iterations + 1 - nu;
	    
	    if(Double.valueOf(result).equals(Double.NaN) || result > maxIterations) {
	    	
	    	result = maxIterations;
	    	
	    }
	    
	    return result;
		
	}

	/**
	 * Returns the number system used to calculate this
	 * <code>EscapeTimeFractal</code>.
	 * 
	 * @return the number system used to calculate this <code>EscapeTimeFractal</code>.
	 */
	Class<T> getType();
	
}
