package com.anchorwatchstudios.chaos.document;

import com.anchorwatchstudios.chaos.document.fractal.Fractal;
import com.anchorwatchstudios.chaos.document.render.RenderMethod;
import com.anchorwatchstudios.chaos.document.shader.Shader;
import com.anchorwatchstudios.chaos.math.Interpolation;

/**
 * A data structure representing one key-frame of a video.
 * Each frame has a fractal, shader, and renderer.
 * 
 * @author Anthony Atella
 */
public class KeyFrame implements Interpolation<KeyFrame>, Cloneable {

	private Fractal fractal;
	private Shader shader;
	private RenderMethod renderMethod;
	
	/**
	 * Constructs a new <code>KeyFrame</code>.
	 */
	public KeyFrame() {
		
		fractal = null;
		shader = null;
		renderMethod = null;
		
	}
	
	/**
	 * Constructs a new<code>KeyFrame</code> with the given
	 * <code>Fractal</code>, <code>Shader</code>, and 
	 * <code>RenderSettings</code>.
	 * 
	 * @param fractal A fractal
	 * @param shader A shader
	 * @param renderSettings Render settings
	 */
	public KeyFrame(Fractal fractal, Shader shader, RenderMethod renderSettings) {
		
		this.fractal = fractal;
		this.shader = shader;
		this.renderMethod = renderSettings;
		
	}
	
	/**
	 * Returns this <code>KeyFrame</code>s <code>Fractal</code>.
	 * 
	 * @return This <code>KeyFrame</code>s <code>Fractal</code>
	 */
	public Fractal getFractal() {

		return fractal;

	}

	/**
	 * Sets this <code>KeyFrame</code>s <code>Fractal</code>
	 * to the given <code>Fractal</code>.
	 * 
	 * @param fractal The fractal
	 */
	public void setFractal(Fractal fractal) {

		this.fractal = fractal;

	}

	/**
	 * Returns this <code>KeyFrame</code>s <code>Shader</code>.
	 * 
	 * @return This <code>KeyFrame</code>s <code>Shader</code>
	 */
	public Shader getShader() {

		return shader;

	}
	
	/**
	 * Sets this <code>KeyFrame</code>s <code>Shader</code>
	 * to the given <code>Shader</code>.
	 * 
	 * @param shader The shader
	 */
	public void setShader(Shader shader) {
		
		this.shader = shader;
		
	}

	/**
	 * Returns this <code>KeyFrame</code>s <code>RenderMethod</code>.
	 * 
	 * @return This <code>KeyFrame</code>s <code>RenderMethod</code>
	 */
	public RenderMethod getRenderMethod() {
		
		return renderMethod;
		
	}

	/**
	 * Sets this <code>KeyFrame</code>s <code>RenderMethod</code>
	 * to the given <code>RenderMethod</code>.
	 * 
	 * @param renderMethod The render method
	 */
	public void setRenderMethod(RenderMethod renderMethod) {
		
		this.renderMethod = renderMethod;
		
	}

	/**
	 * Returns a text description of this <code>KeyFrame</code>
	 * and its components.
	 * 
	 * @return a text description of this <code>KeyFrame</code> and its components.
	 */
	public String getDescription() {

		String result = fractal.getDescription();
		result += shader.getDescription();
		result += renderMethod.getDescription();
		return result;
		
	}
	
	@Override
	public KeyFrame interpolate(KeyFrame keyFrame, double percent) {

		KeyFrame result = new KeyFrame();
		result.setFractal(fractal.interpolate(keyFrame.getFractal(), percent));
		result.setShader(shader.interpolate(keyFrame.getShader(), percent));
		result.setRenderMethod(renderMethod.interpolate(keyFrame.getRenderMethod(), percent));
		return result;

	}

	@Override
	public KeyFrame clone() {
		
		return new KeyFrame(fractal.clone(), shader.clone(), renderMethod.clone());
		
	}
	
}
