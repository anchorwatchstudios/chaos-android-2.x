package com.anchorwatchstudios.chaos.reflect;

import org.reflections.Reflections;

import java.util.Set;

/**
 * Static Java reflection utility methods.
 * 
 * @author Anthony Atella
 */
public class ReflectionUtils {

	/**
	 * Private constructor. Static class.
	 */
	private ReflectionUtils() {
		
	}
	
	/**
	 * Returns an array of simple class names.
	 * 
	 * @param classes An array of classes
	 * @return An array of simple class names
	 */
	public static String[] getSimpleClassNames(Class<?>[] classes) {
		
		String[] result = new String[classes.length];
		
		for(int i = 0;i < classes.length;i++) {
			
			result[i] = classes[i].getSimpleName();
			
		}
		
		return result;
		
	}
	
	/**
	 * Returns an array of classes that extend the given base class
	 * within the given package.
	 * 
	 * @param pckg The package name
	 * @param baseClass The base interface or class
	 * @return An array of classes that extend the given base class
	 */
	public static Class<?>[] getSubClasses(String pckg, Class<?> baseClass) {

		Reflections reflections = new Reflections(pckg);
		Set<?> classes = reflections.getSubTypesOf(baseClass);
		Object[] array = classes.toArray();
		
		for(int i = 0;i < classes.size();i++) {
			
			try {
				
				array[i].getClass().newInstance();
				
			}
			catch (InstantiationException e) {

				classes.remove(array[i]);
				
			} catch (IllegalAccessException e) {

			}
			
		}
		
		return (Class<?>[]) classes.toArray(new Class<?>[] { });

	}
	
	/**
	 * Sorts an array of classes by name.
	 * 
	 * @param c The class array
	 * @return A new class array in alphabetical order
	 */
	public static Class<?>[] sortByName(Class<?>[] c) {
		
		for(int i = 0;i < c.length;i++) {
			
			for(int j = i + 1;j < c.length;j++) {
				
				if(c[i].getSimpleName().compareToIgnoreCase(c[j].getSimpleName()) > 0) {
				
					Class<?> temp = c[i];
					c[i] = c[j];
					c[j] = temp;
					
				}		
				
			}
			
		}
		
		return c;
		
	}
	
}
