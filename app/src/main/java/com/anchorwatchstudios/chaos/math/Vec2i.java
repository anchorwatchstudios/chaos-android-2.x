package com.anchorwatchstudios.chaos.math;

/**
 * A two-dimensional vector with integer precision.
 * 
 * @author Anthony Atella
 */
public class Vec2i implements Cloneable {

	private int x;
	private int y;
	
	/**
	 * Constructs a new <code>Vec2i</code> with zero values.
	 */
	public Vec2i() {
		
		setX(0);
		setY(0);
		
	}
	
	/**
	 * Constructs a new <code>Vec2i</code> with the given values.
	 * 
	 * @param x The x value
	 * @param y The y value
	 */
	public Vec2i(int x, int y) {
		
		this.setX(x);
		this.setY(y);
		
	}

	/**
	 * Returns the x component of this vector.
	 * 
	 * @return The x component of this vector
	 */
	public int getX() {
		
		return x;
		
	}


	/**
	 * Sets the x component of this vector.
	 * 
	 * @param x The x component of this vector
	 */
	public void setX(int x) {
		
		this.x = x;
		
	}


	/**
	 * Returns the y component of this vector.
	 * 
	 * @return The y component of this vector
	 */
	public int getY() {
		
		return y;
		
	}

	/**
	 * Sets the y component of this vector.
	 * 
	 * @param y The y component of this vector
	 */
	public void setY(int y) {
		
		this.y = y;
		
	}
	
	/**
	 * Creates a clone of this vector.
	 */
	public Vec2i clone() {
		
		return new Vec2i(x, y);
		
	}
	
}
