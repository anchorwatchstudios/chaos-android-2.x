package com.anchorwatchstudios.chaos.math;

/**
 * 
 * Represents a number on the complex plane. A complex number
 * has two fields; a and b. These double values represent a real
 * and imaginary number, respectively. The imaginary number can
 * be thought of as a scalar for the imaginary number i = sqrt(-1).
 * Complex numbers can be thought of as a vector represented as
 * <b>v</b> = a + bi.
 * 
 * @author Anthony Atella
 */
public class Complex implements Interpolation<Complex> {

	private double a, b;
	
	/**
	 * Constructs a new <code>Complex</code> with a and b
	 * set to zero.
	 */
	public Complex() {
		
		a = 0;
		b = 0;
		
	}
	
	/**
	 * Constructs a new <code>Complex</code> with the given
	 * values.
	 * 
	 * @param a The real number
	 * @param b The imaginary number
	 */
	public Complex(double a, double b) {
		
		this.a = a;
		this.b = b;
		
	}
	
	/**
	 * Returns the real part of this <code>Complex</code>.
	 * 
	 * @return The real part of the <code>Complex</code>
	 */
	public double getA() {
		
		return a;
		
	}
	
	/**
	 * Sets the real part of this <code>Complex</code> to
	 * the given value.
	 * 
	 * @param a A real number
	 */
	public void setA(double a) {
		
		this.a = a;
		
	}
	
	/**
	 * Returns the imaginary part of this <code>Complex</code>.
	 * 
	 * @return The imaginary part of the <code>Complex</code>
	 */
	public double getB() {
		
		return b;
		
	}
	
	/**
	 * Sets the imaginary part of this <code>Complex</code> to
	 * the given value.
	 * 
	 * @param b An imaginary number
	 */
	public void setB(double b) {
		
		this.b = b;
		
	}
	
	/**
	 * Adds the given <code>Complex</code> to this <code>Complex</code>.
	 * 
	 * @param c A <code>Complex</code> number
	 * @return The sum of the two <code>Complex</code> numbers
	 */
	public Complex add(Complex c) {
		
		return new Complex(a + c.a, b + c.b);
		
	}
	
	/**
	 * Subtracts the given <code>Complex</code> from this <code>Complex</code>.
	 * 
	 * @param c A <code>Complex</code> number
	 * @return The difference between the two <code>Complex</code> numbers
	 */
	public Complex subtract(Complex c) {
		
		return new Complex(a - c.a, b - c.b);
		
	}
	
	/**
	 * Multiplies this <code>Complex</code> by a scalar value.
	 * 
	 * @param s The scalar
	 * @return A new <code>Complex</code> number scaled to the given
	 * value
	 */
	public Complex scalar(double s) {
	
		return new Complex(a * s, b * s);
		
	}
	
	/**
	 * Multiplies the given <code>Complex</code> by this <code>Complex</code>.
	 * 
	 * @param c A <code>Complex</code> number
	 * @return The product of the two <code>Complex</code> numbers
	 */
	public Complex multiply(Complex c) {
		
		return new Complex(a * c.a - b * c.b, a * c.b + b * c.a);
		
	}
	
	/**
	 * Divides this <code>Complex</code> by the given <code>Complex</code>.
	 * 
	 * @param c A <code>Complex</code> number
	 * @return The quotient of the two <code>Complex</code> numbers
	 */
	public Complex divide(Complex c) {
		
		return multiply(c.reciprocal());
		
	}
	
	/**
	 * Returns the conjugate of this <code>Complex</code> number.
	 * 
	 * @return The conjugate of this <code>Complex</code> number
	 */
	public Complex conjugate() {
	
		return new Complex(a, -b);
		
	}
	
	/**
	 * Returns the reciprocal of this <code>Complex</code> number.
	 * 
	 * @return The reciprocal of this <code>Complex</code> number
	 */
	public Complex reciprocal() {
		
		double scale = a * a + b * b;
        return new Complex(a / scale, -b / scale);
		
	}
	
	/**
	 * Raises this <code>Complex</code> to the given power.
	 * 
	 * @param n The power to raise this number to
	 * @return A new <code>Complex</code> number
	 */
 	public Complex pow(int n) {
		
		Complex result = new Complex(a, b);
		
		for(int i = 1;i < n;i++) {
			
			result = result.multiply(this);
			
		}
		
		return result;
		
	}
 	
	/**
	 * Exponentiates this <code>Complex</code> number
	 * using Euler's number.
	 * 
	 * @return A new <code>Complex</code> number
	 */
    public Complex exp() {
    
    	return new Complex(Math.exp(a) * Math.cos(b), Math.exp(a) * Math.sin(b));
   
    }

    /**
     * Returns the sine of this <code>Complex</code> number.
     * 
     * @return The sine of this <code>Complex</code> number
     */
    public Complex sin() {
   
    	return new Complex(Math.sin(a) * Math.cosh(b), Math.cos(a) * Math.sinh(b));
   
    }

    /**
     * Returns the cosine of this <code>Complex</code> number.
     * 
     * @return The cosine of this <code>Complex</code> number
     */
    public Complex cos() {
    	
        return new Complex(Math.cos(a) * Math.cosh(b), -Math.sin(a) * Math.sinh(b));
    
    }

    /**
     * Returns the tangent of this <code>Complex</code> number.
     * 
     * @return The tangent of this <code>Complex</code> number
     */
    public Complex tan() {
    	
        return sin().divide(cos());
        
    }
	
    /**
     * Returns the length of this <code>Complex</code> number.
     * 
     * @return The length of this <code>Complex</code> number
     */
	public double abs() {
		
		return Math.hypot(a, b);
		
	}
	
	/**
	 * Rotates this <code>Complex</code> point about the origin
	 * by the given angle, in radians.
	 * 
	 * @param angle The angle, in radians
	 * @return A new <code>Complex</code> number
	 */
	public Complex rotate(double angle) {
		
		double rc = Math.cos(angle);
		double rs = Math.sin(angle);
		return new Complex(a * rc - b * rs, a * rs + b * rc);
		
	}
	
	/**
	 * Rotates this <code>Complex</code> point about the given
	 * point by the given angle, in radians.
	 * 
	 * @param point The point to rotate about
	 * @param angle The angle, in radians
	 * @return A new <code>Complex</code> number
	 */
	public Complex rotateAbout(Complex point, double angle) {
		
		return subtract(point).rotate(angle).add(point);
		
	}
	
	@Override
    public boolean equals(Object o) {
    	
        if(o == null) {
        	
        	return false;
        
        }
        
        if(getClass() != o.getClass()) {
        	
        	return false;
        
        }
        
        Complex c = (Complex) o;
        return Double.compare(this.a, c.a) == 0 && Double.compare(this.b, c.b) == 0;
   
    }
    
	@Override
    public String toString() {
    	
    	return a + " + " + b + "i";
    	
    }

	@Override
	public Complex interpolate(Complex complex, double percent) {

		Complex result = new Complex();
		result.setA(Interpolation.interpolate(a, complex.getA(), percent));
		result.setB(Interpolation.interpolate(b, complex.getB(), percent));
		return result;
		
	}
        
}
