package com.anchorwatchstudios.chaos.gl;

/**
 * An OpenGL shader program.
 * 
 * @author Anthony Atella
 */
public interface GLShader {

	/**
	 * Returns the program.
	 * 
	 * @return The program
	 */
	String[] getProgram();
	
	/**
	 * Returns the program id.
	 * 
	 * @return The program id
	 */
	int getId();
	
	/**
	 * Sets the program id to the given value.
	 * 
	 * @param id The new id
	 */
	void setId(int id);
	
	/**
	 * Compiles this program. Returns true
	 * if successful.
	 * 
	 * @return True if successful, false otherwise
	 */
	boolean compile();
	
	/**
	 * Returns this shader programs log.
	 * 
	 * @return This shader programs log
	 */
	String getLog();
	
}