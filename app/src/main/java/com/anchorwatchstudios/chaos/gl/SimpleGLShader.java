package com.anchorwatchstudios.chaos.gl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Reads a program from an input stream or string.
 * 
 * @author Anthony Atella
 */
public abstract class SimpleGLShader implements GLShader {

	private String[] program;
	private int id;

	/**
	 * Creates a new <code>SimpleGLShader</code> with an empty program.
	 */
	public SimpleGLShader() {

		program = new String[0];

	}

	/**
	 * Creates a new <code>SimpleGLShader</code> with the given program.
	 * 
	 * @param program The program string array
	 */
	public SimpleGLShader(String[] program) {

		this.program = program;

	}

	/**
	 * Creates a new <code>SimpleGLShader</code> with the given <code>InputStream</code>.
	 * 
	 * @param is The <code>InputStream</code>
	 * @throws IOException If an IO error occurs
	 */
	public SimpleGLShader(InputStream is) throws IOException {

		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		StringBuilder sb = new StringBuilder();
		String line;

		while((line = br.readLine()) != null) {

			sb.append(line + "\n");

		}
		
		is.close();
		
		program = new String[] {sb.toString()};

	}

	/**
	 * Creates a new <code>SimpleGLShader</code> with the given <code>InputStream</code>s.
	 * 
	 * @param is The <code>InputStream</code> array
	 * @throws IOException If an IO error occurs
	 */
	public SimpleGLShader(InputStream[] is) throws IOException {
	
		StringBuilder sb = new StringBuilder();
		String line;
		
		for(int i = 0; i < is.length;i++) {
			
			InputStreamReader isr = new InputStreamReader(is[i]);
			BufferedReader br = new BufferedReader(isr);

			while((line = br.readLine()) != null) {

				sb.append(line + "\n");

			}
			
			is[i].close();
			
		}
		
		program = new String[] {sb.toString()};
		
	}
	
	/**
	 * Sets the program to the given program.
	 * 
	 * @param program The new program string array
	 */
	public void setProgram(String[] program) {

		this.program = program;

	}
	
	@Override
	public String[] getProgram() {

		return program;

	}

	@Override
	public int getId() {

		return id;
	
	}

	@Override
	public void setId(int id) {

		this.id = id;
		
	}

}
