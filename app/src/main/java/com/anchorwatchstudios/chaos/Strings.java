package com.anchorwatchstudios.chaos;

import com.anchorwatchstudios.chaos.android.Chaos;

/**
 * This class holds all the strings in the program for clarity.
 * In the future this class will be removed and replaced with an
 * i18n solution involving <code>ResourceBundle</code>s.
 * 
 * @author Anthony Atella
 */
public class Strings {

	public static final String WINDOW_TITLE = "Chaos";
	public static final String MENU_FILE = "File";
	public static final String MENU_NEW = "New";
	public static final String MENU_OPEN = "Open...";
	public static final String MENU_SAVE = "Save";
	public static final String MENU_SAVE_AS = "Save As...";
	public static final String MENU_EXPORT_IMAGE = "Export Image...";
	public static final String MENU_EXPORT_VIDEO = "Export Video...";
	public static final String MENU_EXIT = "Exit";
	public static final String MENU_EDIT = "Edit";
	public static final String MENU_SETTINGS = "Settings...";
	public static final String MENU_VIEW = "View";
	public static final String MENU_ORBITAL_MODE = "Orbital Mode";
	public static final String MENU_FLY_MODE = "Fly Mode";
	public static final String MENU_FULLSCREEN = "Fullscreen";
	public static final String MENU_WINDOW = "Window";
	public static final String MENU_SHOW_HIDE = "Show / Hide";
	public static final String MENU_TOOLBAR = "Toolbar";
	public static final String MENU_MENU_BAR = "Menu Bar";
	public static final String MENU_ROLLOUT = "Rollout";
	public static final String MENU_STATUS_BAR = "Status Bar";
	public static final String MENU_TOOLBAR_LOCK = "Lock the Toolbar";
	public static final String MENU_HELP = "Help";
	public static final String MENU_CONTENTS = "Contents...";
	public static final String MENU_ABOUT = "About...";
	public static final String FRACTAL_JULIA = "Julia";
	public static final String FRACTAL_MANDELBROT = "Mandelbrot";
	public static final String FRACTAL_NEWTON_BASIN = "Newton-Basin";
	public static final String FRACTAL_TREE = "Tree";
	public static final String FRACTAL_CANTOR = "Cantor";
	public static final String ERROR_PROGRAM_LINK = "A fatal error has occurred: Shader program link error";
	public static final String EDITOR_ITERATIONS = "Iterations";
	public static final String EDITOR_THRESHOLD = "Threshold";
	public static final String EDITOR_CA = "Ca";
	public static final String EDITOR_CB = "Cb";
	public static final String EDITOR_ZA = "Za";
	public static final String EDITOR_ZB = "Zb";
	public static final String EDITOR_DISCRETE = "Discrete";
	public static final String GRAPH_COMPLEX = "Complex Plane";
	public static final String SHADER_J2D_DICHROMATIC = "Dichromatic (Java2D)";
	public static final String SHADER_J2D_RANDOM = "Random (Java2D)";
	public static final String SHADER_GL3_RANDOM = "Random (OpenGL3)";
	public static final String GRAPH_MIN_X = "Min. X";
	public static final String GRAPH_MIN_Y = "Min. Y";
	public static final String GRAPH_MAX_X = "Max. X";
	public static final String GRAPH_MAX_Y = "Max. Y";
	public static final String CONFIRM_CLOSE = "Would you like to save your changes?";
	public static final String MENU_CLOSE = "Close";
	public static final String MESSAGE_RENDERING = "Rendering...";
	public static final String MESSAGE_READY = "Ready";
	public static final String COLOR = "Color";
	public static final String EDITOR_NA = "Na";
	public static final String EDITOR_NB = "Nb";
	public static final String EDITOR_ALPHAA = "Alpha A";
	public static final String EDITOR_ALPHAB = "Alpha B";
	public static final String TIMELINE = "Timeline";
	public static final String MENU_TIMELINE = "Timeline";
	public static final String MENU_DEBUG = "Debug";
	public static final String EDITOR_RED = "Red";
	public static final String EDITOR_INVERT = "Invert";
	public static final String MENU_CAMERA = "Camera";
	public static final String MENU_RENDER_IMP = "Render Implementation";
	public static final String EDITOR_LENGTH = "Length";
	public static final String EDITOR_ANGLE = "Angle";
	public static final String EDITOR_STEP = "Step";
	public static final String EDITOR_SPACE = "Space";
	public static final String EDITOR_CC = "Cc";
	public static final String EDITOR_CD = "Cd";
	public static final String FRACTAL_JULIABULB = "Juliabulb";
	public static final String EDITOR_ZC = "Zc";
	public static final String EDITOR_ZD = "Zd";
	public static final String FRACTAL_MANDELBULB = "Mandelbulb";
	public static final String EDITOR_RAY_THRESHOLD = "Ray Threshold";
	public static final String EDITOR_RESOLUTION = "Document Resolution";
	public static final String EDITOR_WIDTH = "Width";
	public static final String EDITOR_HEIGHT = "Height";
	public static final String IO_ERROR = "An error occurred while trying to writing your file! :(";
	public static final String GL_ERROR = "An OpenGL error occurred! :( Error code: ";
	public static final String ABOUT_MESSAGE = "<html><center><b>Chaos " + Chaos.VERSION_MAJOR + "." + Chaos.VERSION_MINOR + "." + Chaos.VERSION_PATCH + "<br/>&copy 2017</b><br/>AnchorWatch Studios<br/><a href='https://anchorwatchstudios.com'>www.anchorwatchstudios.com</a></center></html>";
	public static final String ABOUT_MESSAGE_TITLE = "About Chaos...";
	public static final String BROWSER_ERROR = "Failed to open the browser. :( For help go to https://anchorwatchstudios.com/chaos";
	public static final String EDITOR_N = "n";
	public static final String EDITOR_VIDEO = "Video";
	public static final String EDITOR_FPK = "Frames per key-frame";
	public static final String EDITOR_FPS = "Frames per second";
	public static final String FRACTAL_MANDELBOX = "Mandelbox";
	public static final String EDITOR_FOLD_LIMIT = "Fold Limit";
	public static final String EDITOR_FIXED_RADIUS = "Fixed Radius";
	public static final String EDITOR_MIN_RADIUS = "Min. Radius";
	public static final String EDITOR_SCALE = "Scale";
	public static final String PLEASE_WAIT = "Please wait...";
	public static final String EXPORTING_VIDEO = "Exporting Video";
	public static final String DICHROMATIC = "dichromatic";
	public static final String MONOCHROMATIC = "monochromatic";
	public static final String RANDOM = "random";
	public static final String MIN_DISTANCE = "min-distance";
	public static final String GL_ERROR_VERSION = "Chaos requires OpenGL4 or higher. Please update your graphics driver, or use a different machine.";
	public static final String CONTROLS_MESSAGE_TITLE = "Controls";
	public static final String MENU_CONTROLS = "Controls";
	
	/**
	 * Hidden. This class is static.
	 */
	private Strings() {
		
	}
	
}
