/**
 * Triplex number operations.
 *
 * @author Anthony Atella
 */

/**
 * Raises a given triplex number to the given power.
 * Based on the White and Nylander formula.
 *
 * @param t The triplex number
 * @param n The power to raise the triplex to
 * @return A new triplex number
 */
vec3 triplexPower(vec3 t, float n) {
	// Convert to spherical coordinates
	float rho = length(t);
    float theta = asin(t.z / rho);
    float phi = atan(t.y, t.x);
    // Scale and rotate the point
    rho = pow(rho, n);
    theta *= n;
    phi *= n;
    // Raise the vector t to the nth power using White and Nylander's formula (Backwards??)
    float cosTheta = cos(theta);
    return rho * vec3(cosTheta * cos(phi), cosTheta * sin(phi), sin(theta));
}

/**
 * Raises a given triplex number to the given power.
 * Based on the White and Nylander formula. The length
 * is supplied as an argument here even though we can
 * derive it from the given triplex because it is
 * convenient to compute it elsewhere.
 *
 * @param t The triplex number
 * @param n The power to raise the triplex to
 * @param r The length of the triplex number (radius from origin)
 * @return A new triplex number
 */
vec3 triplexPower(vec3 t, float n, float r) {
	// Convert to spherical coordinates
    float theta = asin(t.z / r);
    float phi = atan(t.y, t.x);
    // Scale and rotate the point
    float rho = pow(r, n);
    theta *= n;
    phi *= n;
    // Raise the vector t to the nth power using White and Nylander's formula (Backwards??)
    float cosTheta = cos(theta);
    return rho * vec3(cosTheta * cos(phi), cosTheta * sin(phi), sin(theta));
}
