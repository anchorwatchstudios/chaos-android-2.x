/**
 * Complex number operations.
 *
 * @author Anthony Atella
 */

/**
 * Returns a Cartesian coordinate from the given
 * polar coordinate.
 *
 * @param r The radius
 * @param a The angle in radians
 * @return A point
 */
vec2 polar(float r, float a) {
	return vec2(cos(a) * r, sin(a) * r);
}

/**
 * Returns the magnitude of a complex number.
 *
 * @param c The complex number
 * @return The magnitude of the complex number
 */
float complexMagnitude(vec2 c) {
	return pow(length(c), 2.0);
}

/**
 * Returns a complex argument in the form of
 * the angle in radians from the origin.
 *
 * @param c The complex number
 * @return An angle in radians
 */
float complexArgument(vec2 c) {
	return atan(c.y, c.x);
}

/**
 * Returns the given complex number raised
 * to the given power without using a loop.
 *
 * @param c The complex number
 * @param n The power
 * @result A new complex number
 */
vec2 complexPower(vec2 c, float n) {
	float r = pow(length(c), n);
	float a = complexArgument(c) * n;
	return polar(r, a);
}

/**
 * Returns the product of the two given complex
 * numbers.
 *
 * @param c1 A complex number
 * @param c2 A complex number
 * @return The product of the two given complex numbers
 */
vec2 complexMultiply(vec2 c1, vec2 c2) {
	return vec2(c1.x * c2.x - c1.y * c2.y, c1.x * c2.y + c1.y * c2.x);
}

/**
 * Returns the reciprocal of the given complex number.
 *
 * @param c The complex number
 * @return The reciprocal of the given complex number
 */
vec2 complexReciprocal(vec2 c) {
	return vec2(c.x / complexMagnitude(c), -c.y / complexMagnitude(c));
}

/**
 * Returns the quotient of the two given complex
 * numbers.
 *
 * @param c1 A complex number
 * @param c2 A complex number
 * @return The quotient of the two given complex numbers
 */
vec2 complexDivide(vec2 c1, vec2 c2) {
	return complexMultiply(c1, complexReciprocal(c2));
}
