/**
 * Renders the Mandelbrot set.
 *
 * @author Anthony Atella
 */
#define FRACTAL_ITERATIONS 25
uniform bool discrete;
uniform vec2 z;
uniform float fractalThreshold;

/**
 * Returns the precise number of iterations from
 * a point and a discrete result.
 *
 * @param c The point
 * @param result The discrete iteration results
 * @return The precise iterations
 */
float getPreciseIterations(vec2 c, float result) {
	float log_zn = log(length(c)) / 2.0;
	float nu = log(log_zn / log(2.0)) / log(2.0);
	return result + 1.0 - nu;
}

/**
 * Returns a vector containing data about the point.
 *
 * @param point The complex point to check
 * @return A vector: vec4(iterations, distance, minDistance, magnitude)
 */
vec4 isInSet(vec2 c) {
	float result = 0.0;
	vec2 zee = z;
	float minDistance = 1000.0;

	for(int i = 0;i < int(FRACTAL_ITERATIONS); i++) {
		zee = complexPower(zee, 2.0) + c;
		float l = length(zee);
		minDistance = min(minDistance, l);

		if(l > pow(fractalThreshold, 2.0)) {
			break;
		}

		result++;
	}

	if(!discrete && result < float(FRACTAL_ITERATIONS)) {
		result = getPreciseIterations(zee, result);
	}

	float length = length(zee);
	return vec4(result / float(FRACTAL_ITERATIONS), length, minDistance, 0.33 * log(length * length) + 1.0);
}
