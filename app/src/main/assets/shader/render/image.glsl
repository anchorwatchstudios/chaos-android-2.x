#version 410
precision highp float;

/**
 * Draws a fullscreen image, respecting aspect ratio. The result
 * is equivalent to the css, "background-size: fit;".
 *
 * @author Anthony Atella
 */
uniform sampler2D tex;
uniform vec2 resolution;
uniform vec2 canvasSize;

/**
 * The main entry point.
 */
void main() {
	vec2 pos = gl_FragCoord.xy;
	float canvasAspect = canvasSize.y / canvasSize.x;
	float resAspect = resolution.y / resolution.x;
	float aspectRatio = resAspect / canvasAspect;

	// Canvas is wider
	if(canvasAspect < resAspect) {

		float scale = canvasSize.y / resolution.y;
		float scaledWidth = scale * resolution.x;
		pos.x -= (canvasSize.x - scaledWidth) / 2.0;

		/*
		// Image is larger
		if(canvasSize.y < resolution.y) {

			//TODO: Cases where image is wider than view
			float scaledWidth = scale * resolution.x;
			pos.x -= (canvasSize.x - scaledWidth) / 2.0;

		}
		// Canvas is larger
		else {

			float scaledWidth = scale * resolution.x;
			pos.x -= (canvasSize.x - scaledWidth) / 2.0;

		}
		*/

	}
	// Image is wider
	else if(canvasAspect > resAspect) {

		float scale = canvasSize.x / resolution.x;
		float scaledHeight = scale * resolution.y;
		pos.y -= (canvasSize.y - scaledHeight) / 2.0;

		/*
		// Image is larger
		if(canvasSize.x < resolution.x) {

			float scaledHeight = scale * resolution.y;
			pos.y -= (canvasSize.y - scaledHeight) / 2.0;

		}
		// Canvas is larger
		else {

			float scaledHeight = scale * resolution.y;
			pos.y -= (canvasSize.y - scaledHeight) / 2.0;

		}
		*/

	}

	pos.x *= aspectRatio;
	pos /= canvasSize;
	if (pos.x < 0 || pos.x > 1 || pos.y < 0 || pos.y > 1) discard;
	gl_FragColor = texture(tex, pos);
}
