#version 100
precision highp float;

/**
 * Complex plotter. Plots points on the complex plane.
 *
 * @author Anthony Atella
 */
uniform vec2 resolution;
uniform mat2 cameraPos;
uniform float rotation;

/**
 * Returns a vector containing data about the point.
 *
 * @param point The complex point to check
 * @return A vector: vec4(iterations, distance, minDistance, magnitude)
 */
vec4 isInSet(vec2 point);

/**
 * Returns a color from a vec4(iterations, distance, minDistance, magnitude).
 *
 * @param value A vector: vec4(iterations, distance, minDistance, magnitude)
 * @return vec4 A color
 */
vec4 getColor(vec4 result);

/**
 * Returns the coordinates of the given fragment in screen space,
 * respecting aspect ratio, and camera rotation and translation.
 *
 * @param pixel The gl_FragCoord
 * @param imageSize The image resolution
 * @param cameraPos The position of the camera
 * @param rotation The rotation of the camera
 * @return A point in screen space
 */
vec2 getScreenCoords(vec2 pixel, vec2 imageSize, mat2 cameraPos, float rotation) {
	float a = pixel.x * (cameraPos[1].x - cameraPos[0].x) / imageSize.x + cameraPos[0].x;
	a *= imageSize.x / imageSize.y;
	float b = pixel.y * (cameraPos[0].y - cameraPos[1].y) / imageSize.y + cameraPos[1].y;
	float rc = cos(rotation);
    float rs = sin(rotation);
    vec2 translation = vec2(cameraPos[0].x + (cameraPos[1].x - cameraPos[0].x) / 2.0, cameraPos[1].y + (cameraPos[0].y - cameraPos[1].y) / 2.0);
    a -= translation.x;
    b -= translation.y;
    vec2 result = vec2(a * rc - b * rs, a * rs + b * rc);
    result.x += translation.x;
    result.y += translation.y;
	return result;
}

/**
 * The main entry point. Plots a point on the complex plane.
 */
void main() {
	vec2 point = getScreenCoords(gl_FragCoord.xy, resolution, cameraPos, rotation);
	vec4 result = isInSet(point);
	gl_FragColor = getColor(result);
}
