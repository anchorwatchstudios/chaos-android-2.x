/**
 * Minimum distance fragment shader. Accents the minimum
 * distance with a color, while coloring the base color with
 * the iteration count.
 *
 * Concepts taken from
 * http://hirnsohle.de/test/fractalLab/
 *
 * @author Anthony Atella
 */
uniform vec4 color1;
uniform vec4 color2;
uniform vec4 color3;
uniform bool inverted;

/**
 * Returns a color from a vec4(iterations, distance, minDistance, magnitude).
 *
 * @param value A vector: vec4(iterations, distance, minDistance, magnitude)
 * @return vec4 A color
 */
vec4 getColor(vec4 value) {
    if(inverted) {
    	value.x = 1.0 - value.x;
    }
    vec4 result = color1;
    if(value.x > 0) {
    	result = mix(color1, mix(color2, color3, value.x * value.z), value.x);
    }
    return result;
}
