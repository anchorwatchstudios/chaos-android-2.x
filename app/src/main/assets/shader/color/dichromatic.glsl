/**
 * Dichromatic fragment shader. Returns the
 * first color if the value is 0, otherwise
 * returns the second color times the given
 * value.
 *
 * @author Anthony Atella
 */
uniform vec4 color1;
uniform vec4 color2;
uniform bool inverted;

/**
 * Returns a color from a vec4(iterations, distance, minDistance, magnitude).
 *
 * @param value A vector: vec4(iterations, distance, minDistance, magnitude)
 * @return vec4 A color
 */
vec4 getColor(vec4 value) {
	if(inverted) {
		value.x = 1.0 - value.x;
	}
	float inverted = 1.0 - value.x;
	float red1 = color1.x * value.x;
	float green1 = color1.y * value.x;
	float blue1 = color1.z * value.x;
	float red2 = color2.x * inverted;
	float green2 = color2.y * inverted;
	float blue2 = color2.z * inverted;
	return vec4(red1 + red2 / 2, green1 + green2 / 2, blue1 + blue2 / 2, 1.0);
}
